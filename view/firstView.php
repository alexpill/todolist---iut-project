<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> TellMeWhatTODO. </title>
    <link rel="stylesheet" href="view/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="view/css/global.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Megrim" rel="stylesheet">
</head>
<body>
        <main>
            <canvas id="canvas_boom"></canvas>
            <nav class="navbar navbar-light">
                <a class="navbar-brand" href="index.php#"><p class="display-3" id="todo_title">TellMeWhatTODO</p></a>
                    <p class="display-3" id="todo_title">TellMeWhatTODO</p>
                    <div class="">
                        <?php
                        if(!isset($a) && !isset($u)){
                            echo "<div class='container align-content-start'>";
                            echo "<div class='row justify-content-end'>";
                            echo "<a class=\"text-center h5 btn-secondary p-1 rounded mr-1\" href=\"./index.php?action=viewConnectionAdmin\">Connection Admin</a>";
                            echo "</div>";
                            echo "<div class='row justify-content-end'>";
                            echo "<a class=\"text-center h5 btn-secondary p-1 rounded ml-1\" href=\"./index.php?action=viewConnectionUser\">Connection User</a>";
                            echo "</div>";
                            echo "<div class='row justify-content-end'>";
                            echo "<a class=\"text-center h5 btn-secondary p-1 rounded \" href=\"./index.php?action=viewInscriptionUser\">Inscription</a>";
                            echo "</div>";
                            echo "</div>";
                        }
                        ?>
                        <?php
                        if(isset($a)){
                            echo "<div>";
                            echo "<a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionAdmin\">Déconnexion ".$a->getRole()." ".$a->getLogin()."</a>";
                            echo "</div>";
                        }
                        if(isset($u)){
                            echo "<div>";
                            echo "<a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionUser\">Déconnexion ".$u->getRole()." ".$u->getLogin()."</a>";
                            echo "</div>";
                        }
                        ?>
                    </div>
            </nav>

            <div id="main">


                <?php
                if(isset($taskLists))
                {
                    echo "<h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>";
                    if(isset($totTaskList) && $totTaskList != 0){
                        echo "<form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">";
                        echo "<div class='container-fluid card-body w-auto'>";
                        echo "<div class='row'>";
                        foreach ($taskLists as $taskList)
                        {
                            echo "<div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>";
                            echo "<a href='index.php?action=viewList&id=$taskList->id' class='text-center text-light pubTaskList".$taskList->id."'><h3>" . $taskList->title . "</h3></a>";
                            echo "<h4 class='text-center pubTaskList".$taskList->id."'>" . $taskList->comment . "</h4>";
                            echo "<a href=\"./index.php?action=delPublic&id=".$taskList->id."\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>";
                            echo '<input type="checkbox" class="checkbox ml-4 align-content-center" id="completeTaskList'.$taskList->id.'" name="completeTaskList'.$taskList->id.'"';
                            if($taskList->completed){
                                echo ' checked>';
                                echo "<style type='text/css'> .pubTaskList".$taskList->id." { text-decoration: line-through;} </style>";
                            }
                            else{
                                echo '>';
                            }
                            echo "<kbd class='ml-1' for=\"completeTaskList'.$taskList->id.\">Compléter</kbd>";
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        echo "<div class='text-center'>";
                        echo "<button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste publique</button>";
                        echo "<input type=\"hidden\" name=\"action\" value=\"valPublic\">";
                        echo "<input type=\"hidden\" name=\"p\" value=\"$page\">";
                        echo "</div>";
                        echo "</form>";
                    }
                else{
                    echo "<h3 align='center'>Pas de liste publiques</h3>";
                    }


                }
                ?>
                <div align="center" class="container">
                    <nav class="pagination justify-content-center w-100">
                        <?php
                        if(isset($totTaskList) && $totTaskList != 0) {
                            if(isset($page)) {
                                if ($page != 1) {
                                    echo "<li class='page-item'><a href=\"index.php?p=1".(isset($pageP)?"&pp=$pageP":"")."\" class='page-link'>1  </a></li>";
                                    echo "<li class='page-item'><a href=\"index.php?p=".($page - 1).(isset($pageP)?"&pp=$pageP":"")."\"class='page-link'> < </a></li>";
                                }
                                echo "<span class='page-link'>$page</span>";
                                if ($page != $nbPage) {
                                    echo "<li class='page-item'><a href=\"index.php?p=".($page + 1).(isset($pageP)?"&pp=$pageP":"")."\" class='page-link'> > </a></li>";
                                    echo "<li class='page-item'><a href=\"index.php?p=".($nbPage).(isset($pageP)?"&pp=$pageP":"")."\" class='page-link'>".$nbPage."</a></li>";
                                }
                            }
                        }
                        ?>
                    </nav>
                </div>
                <div class="text-center mt-5">
                    <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=viewAdd">Ajout d'une liste</a>
                </div>

                <?php
                if(isset($taskListsP) && isset($u))
                {
                    echo "<h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de ".$u->getLogin()." </h2>";
                    if(isset($totTaskListP) && $totTaskListP != 0){
                        echo "<form action=\"index.php\" method=\"post\" name=\"formTaskListPrivate\">";
                        echo "<div class='container-fluid card-body w-auto'>";
                        echo "<div class='row'>";
                        foreach ($taskListsP as $taskList)
                        {
                            echo "<div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>";
                            echo "<a href='index.php?action=viewListPrivate&id=$taskList->id' class='text-center text-light priTaskList".$taskList->id."'><h3>" . $taskList->title . "</h3></a>";
                            echo "<h4 class='text-center priTaskList".$taskList->id."'>" . $taskList->comment . "</h4>";
                            echo "<a href=\"./index.php?action=delPrivate&id=".$taskList->id."\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>";
                            echo '<input type="checkbox" class="checkbox ml-4" id="completeTaskListPri'.$taskList->id.'" name="completeTaskListPri'.$taskList->id.'"';
                            if($taskList->completed){
                                echo ' checked>';
                                echo "<style type='text/css'> .priTaskList".$taskList->id." { text-decoration: line-through;} </style>";
                            }
                            else{
                                echo '>';
                            }
                            echo '<kbd class="ml-1" for="completeTaskListPri'.$taskList->id.'">Compléter</kdb>';
                            echo "</div>";
                        }
                        echo "</div>";
                        echo "</div>";
                        echo "<div class='text-center'>";
                        echo "<button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste Privée</button>";
                        echo "<input type=\"hidden\" name=\"action\" value=\"valPrivate\">";
                        echo "<input type=\"hidden\" name=\"pp\" value=\"$pageP\">";
                        echo "</div>";
                        echo "</form>";
                    }
                    else{
                        echo "<h3 align='center'>Pas de listes privées</h3>";
                    }


                }
                ?>
                <div align="center" class="container">
                    <nav class="pagination justify-content-center w-100">
                        <?php
                        if(isset($totTaskListP) && $totTaskListP != 0) {
                            if(isset($pageP)) {
                                if ($pageP != 1) {
                                    echo "<li class='page-item'><a href=\"index.php?pp=1&p=".(isset($page)?$page:1)."#privateTaskListDisplay\" class='page-link'>1  </a></li>";
                                    echo "<li class='page-item'><a href=\"index.php?pp=".($pageP - 1)."&p=".(isset($page)?$page:1)."#privateTaskListDisplay\" class='page-link'> < </a></li>";
                                }
                                echo "<span class='page-link'>$pageP</span>";
                                if ($pageP != $nbPageP) {
                                    echo "<li class='page-item'><a href=\"index.php?pp=".($pageP + 1)."&p=".(isset($page)?$page:1)."#privateTaskListDisplay\" class='page-link'> > </a></li>";
                                    echo "<li class='page-item'><a href=\"index.php?pp=".($nbPageP)."&p=".(isset($page)?$page:1)."#privateTaskListDisplay\" class='page-link'>".$nbPageP."</a></li>";
                                }
                            }
                        }
                        ?>
                    </nav>
                </div>
                <?php
                if(isset($u)){
                    echo '<div class="text-center mt-5">';
                    echo '<a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=viewAddPrivate">Ajout d\'une liste</a>';
                    echo '</div>';
                }
                ?>

            </div>

<!--            <script type="text/javascript" src="view/js/particles.js"></script>-->
<!--            <script type="text/javascript" src="view/js/app.js"></script>-->
<!--            <script type="text/javascript" src="view/js/particle1.js"></script> -->
                <script type="text/javascript" src="view/js/particle2.js"></script>
        </main>
    </body>
</html>
