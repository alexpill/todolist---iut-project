<?php

/* firstTemplate.php */
class __TwigTemplate_a729dbcfcf28b85f21f69411921cd0c4078cd1960ece0a31f156ebabcd9ba293 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<!-- ... -->
<body>
<h1>";
        // line 4
        echo twig_escape_filter($this->env, ($context["msg"] ?? null));
        echo "</h1>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "firstTemplate.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "firstTemplate.php", "C:\\wamp64\\www\\to_do_list_php\\view\\template\\firstTemplate.php");
    }
}
