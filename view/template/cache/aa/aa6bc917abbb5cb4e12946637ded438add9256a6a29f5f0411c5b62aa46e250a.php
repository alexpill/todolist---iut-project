<?php

/* firstTemplate.php */
class __TwigTemplate_0898911fb05e6daf3e20aac67abdff9a777aedb98fde34f929e37848f357fc1b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title> TellMeWhatTODO. </title>
    <link rel=\"stylesheet\" href=\"view/css/bootstrap.min.css\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"view/css/global.css\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Megrim\" rel=\"stylesheet\">
</head>
<body>
<main>
    <canvas id=\"canvas_boom\"></canvas>
    <nav class=\"navbar navbar-light\">
        <a class=\"navbar-brand\" href=\"#\">
            <p class=\"display-3\" id=\"todo_title\">TellMeWhatTODO</p>
            <div class=\"\">
                ";
        // line 18
        if (( !(isset($context["a"]) || array_key_exists("a", $context)) &&  !(isset($context["u"]) || array_key_exists("u", $context)))) {
            // line 19
            echo "                    <div class='container align-content-start'>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded mr-1\" href=\"./index.php?action=viewConnectionAdmin\">Connection Admin</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded ml-1\" href=\"./index.php?action=viewConnectionUser\">Connection User</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded \" href=\"./index.php?action=viewInscriptionUser\">Inscription</a>
                    </div>
                    </div>
                ";
        }
        // line 31
        echo "                ";
        if ((isset($context["a"]) || array_key_exists("a", $context))) {
            // line 32
            echo "                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionAdmin\">Déconnexion ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["a"] ?? null), "role", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["a"] ?? null), "login", array()), "html", null, true);
            echo "</a>
                    </div>
                ";
        }
        // line 36
        echo "
                ";
        // line 37
        if ((isset($context["u"]) || array_key_exists("u", $context))) {
            // line 38
            echo "                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionUser\">Déconnexion ";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "role", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "login", array()), "html", null, true);
            echo "</a>
                    </div>
                ";
        }
        // line 42
        echo "            </div>
        </a>
    </nav>

    <div id=\"main\">
        ";
        // line 47
        if ((isset($context["taskLists"]) || array_key_exists("taskLists", $context))) {
            // line 48
            echo "            <h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>
            ";
            // line 49
            if (((isset($context["totTaskList"]) || array_key_exists("totTaskList", $context)) && (($context["totTaskList"] ?? null) != 0))) {
                // line 50
                echo "                <form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">
                    <div class='container-fluid card-body w-auto'>
                        <div class='row'>
                ";
                // line 53
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["taskLists"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["taskList"]) {
                    // line 54
                    echo "                            <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                                <a href='index.php?action=viewList&id=";
                    // line 55
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "' class='text-center text-light pubTaskList";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'><h3>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "title", array()), "html", null, true);
                    echo "</h3></a>
                                <h4 class='text-center pubTaskList";
                    // line 56
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "comment", array()), "html", null, true);
                    echo "</h4>
                                <a href=\\\"./index.php?action=delPublic&id=";
                    // line 57
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\\\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList'.\$taskList->id.'\" name=\"completeTaskList'.\$taskList->id.\">
                                <kbd class='ml-1' for=\"completeTaskList";
                    // line 59
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\">Compléter</kbd>
                            </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['taskList'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 62
                echo "                        </div>
                    </div>
                    <div class='text-center'>
                        <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste publique</button>
                        <input type=\"hidden\" name=\"action\" value=\"valPublic\">
                        <input type=\"hidden\" name=\"p\" value=";
                // line 67
                echo twig_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
                echo "\">
                    </div>
                </form>
            ";
            }
            // line 71
            echo "        ";
        } else {
            // line 72
            echo "            <h3 align='center'>Pas de liste publiques</h3>
        ";
        }
        // line 74
        echo "
        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
<!--
                if(isset(\$totTaskList) && \$totTaskList != 0) {
                    if(isset(\$page)) {
                        if (\$page != 1) {
                            echo \"<li class='page-item'><a href=\\\"index.php?p=1\".(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'>1  </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$page - 1).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\"class='page-link'> < </a></li>\";
                        }
                        echo \"<span class='page-link'>\$page</span>\";
                        if (\$page != \$nbPage) {
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$page + 1).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'> > </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$nbPage).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'>\".\$nbPage.\"</a></li>\";
                        }
                    }
                }-->
            </nav>
        </div>
        <div class=\"text-center mt-5\">
            <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAdd\">Ajout d'une liste</a>
        </div>

        <!--
        if(isset(\$taskListsP) && isset(\$u))
        {
            echo \"<h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de \".\$u->getLogin().\" </h2>\";
            if(isset(\$totTaskListP) && \$totTaskListP != 0){
                echo \"<form action=\\\"index.php\\\" method=\\\"post\\\" name=\\\"formTaskListPrivate\\\">\";
                echo \"<div class='container-fluid card-body w-auto'>\";
                echo \"<div class='row'>\";
                foreach (\$taskListsP as \$taskList)
                {
                    echo \"<div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>\";
                    echo \"<a href='index.php?action=viewListPrivate&id=\$taskList->id' class='text-center text-light priTaskList\".\$taskList->id.\"'><h3>\" . \$taskList->title . \"</h3></a>\";
                    echo \"<h4 class='text-center priTaskList\".\$taskList->id.\"'>\" . \$taskList->comment . \"</h4>\";
                    echo \"<a href=\\\"./index.php?action=delPrivate&id=\".\$taskList->id.\"\\\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>\";
                    echo '<input type=\"checkbox\" class=\"checkbox ml-4\" id=\"completeTaskListPri'.\$taskList->id.'\" name=\"completeTaskListPri'.\$taskList->id.'\"';
                    if(\$taskList->completed){
                        echo ' checked>';
                        echo \"<style type='text/css'> .priTaskList\".\$taskList->id.\" { text-decoration: line-through;} </style>\";
                    }
                    else{
                        echo '>';
                    }
                    echo '<kbd class=\"ml-1\" for=\"completeTaskListPri'.\$taskList->id.'\">Compléter</kdb>';
                    echo \"</div>\";
                }
                echo \"</div>\";
                echo \"</div>\";
                echo \"<div class='text-center'>\";
                echo \"<button type=\\\"submit\\\" class=\\\"text-center h5 btn-primary p-1 rounded btn\\\">Valider complétion de liste Privée</button>\";
                echo \"<input type=\\\"hidden\\\" name=\\\"action\\\" value=\\\"valPrivate\\\">\";
                echo \"<input type=\\\"hidden\\\" name=\\\"pp\\\" value=\\\"\$pageP\\\">\";
                echo \"</div>\";
                echo \"</form>\";
            }
            else{
                echo \"<h3 align='center'>Pas de listes privées</h3>\";
            }


            //TODO : je veux ca https://vincentgarreau.com/particles.js/
        }-->
        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">

              <!--  if(isset(\$totTaskListP) && \$totTaskListP != 0) {
                    if(isset(\$pageP)) {
                        if (\$pageP != 1) {
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=1&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'>1  </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$pageP - 1).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'> < </a></li>\";
                        }
                        echo \"<span class='page-link'>\$pageP</span>\";
                        if (\$pageP != \$nbPageP) {
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$pageP + 1).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'> > </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$nbPageP).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'>\".\$nbPageP.\"</a></li>\";
                        }
                    }
                }
                -->
            </nav>
        </div>
        <!--
        if(isset(\$u)){
            echo '<div class=\"text-center mt-5\">';
            echo '<a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAddPrivate\">Ajout d\\'une liste</a>';
            echo '</div>';
        }
        -->

    </div>

    <!--            <script type=\"text/javascript\" src=\"view/js/particles.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/app.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/particle1.js\"></script> -->
    <script type=\"text/javascript\" src=\"view/js/particle2.js\"></script>
</main>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "firstTemplate.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 74,  159 => 72,  156 => 71,  149 => 67,  142 => 62,  133 => 59,  128 => 57,  122 => 56,  114 => 55,  111 => 54,  107 => 53,  102 => 50,  100 => 49,  97 => 48,  95 => 47,  88 => 42,  80 => 39,  77 => 38,  75 => 37,  72 => 36,  64 => 33,  61 => 32,  58 => 31,  44 => 19,  42 => 18,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title> TellMeWhatTODO. </title>
    <link rel=\"stylesheet\" href=\"view/css/bootstrap.min.css\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"view/css/global.css\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Megrim\" rel=\"stylesheet\">
</head>
<body>
<main>
    <canvas id=\"canvas_boom\"></canvas>
    <nav class=\"navbar navbar-light\">
        <a class=\"navbar-brand\" href=\"#\">
            <p class=\"display-3\" id=\"todo_title\">TellMeWhatTODO</p>
            <div class=\"\">
                {% if a is not defined and u is not defined %}
                    <div class='container align-content-start'>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded mr-1\" href=\"./index.php?action=viewConnectionAdmin\">Connection Admin</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded ml-1\" href=\"./index.php?action=viewConnectionUser\">Connection User</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded \" href=\"./index.php?action=viewInscriptionUser\">Inscription</a>
                    </div>
                    </div>
                {% endif %}
                {% if a is defined %}
                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionAdmin\">Déconnexion {{ a.role }} {{ a.login }}</a>
                    </div>
                {% endif %}

                {% if u is defined %}
                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionUser\">Déconnexion {{ u.role }} {{ u.login }}</a>
                    </div>
                {% endif %}
            </div>
        </a>
    </nav>

    <div id=\"main\">
        {% if taskLists is defined %}
            <h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>
            {% if totTaskList is defined and totTaskList != 0 %}
                <form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">
                    <div class='container-fluid card-body w-auto'>
                        <div class='row'>
                {% for taskList in taskLists %}
                            <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                                <a href='index.php?action=viewList&id={{taskList.id}}' class='text-center text-light pubTaskList{{taskList.id}}'><h3>{{taskList.title}}</h3></a>
                                <h4 class='text-center pubTaskList{{taskList.id}}'>{{taskList.comment}}</h4>
                                <a href=\\\"./index.php?action=delPublic&id={{taskList.id}}\\\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList'.\$taskList->id.'\" name=\"completeTaskList'.\$taskList->id.\">
                                <kbd class='ml-1' for=\"completeTaskList{{taskList.id}}\">Compléter</kbd>
                            </div>
                {% endfor %}
                        </div>
                    </div>
                    <div class='text-center'>
                        <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste publique</button>
                        <input type=\"hidden\" name=\"action\" value=\"valPublic\">
                        <input type=\"hidden\" name=\"p\" value={{page}}\">
                    </div>
                </form>
            {% endif %}
        {% else %}
            <h3 align='center'>Pas de liste publiques</h3>
        {% endif %}

        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
<!--
                if(isset(\$totTaskList) && \$totTaskList != 0) {
                    if(isset(\$page)) {
                        if (\$page != 1) {
                            echo \"<li class='page-item'><a href=\\\"index.php?p=1\".(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'>1  </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$page - 1).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\"class='page-link'> < </a></li>\";
                        }
                        echo \"<span class='page-link'>\$page</span>\";
                        if (\$page != \$nbPage) {
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$page + 1).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'> > </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?p=\".(\$nbPage).(isset(\$pageP)?\"&pp=\$pageP\":\"\").\"\\\" class='page-link'>\".\$nbPage.\"</a></li>\";
                        }
                    }
                }-->
            </nav>
        </div>
        <div class=\"text-center mt-5\">
            <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAdd\">Ajout d'une liste</a>
        </div>

        <!--
        if(isset(\$taskListsP) && isset(\$u))
        {
            echo \"<h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de \".\$u->getLogin().\" </h2>\";
            if(isset(\$totTaskListP) && \$totTaskListP != 0){
                echo \"<form action=\\\"index.php\\\" method=\\\"post\\\" name=\\\"formTaskListPrivate\\\">\";
                echo \"<div class='container-fluid card-body w-auto'>\";
                echo \"<div class='row'>\";
                foreach (\$taskListsP as \$taskList)
                {
                    echo \"<div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>\";
                    echo \"<a href='index.php?action=viewListPrivate&id=\$taskList->id' class='text-center text-light priTaskList\".\$taskList->id.\"'><h3>\" . \$taskList->title . \"</h3></a>\";
                    echo \"<h4 class='text-center priTaskList\".\$taskList->id.\"'>\" . \$taskList->comment . \"</h4>\";
                    echo \"<a href=\\\"./index.php?action=delPrivate&id=\".\$taskList->id.\"\\\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>\";
                    echo '<input type=\"checkbox\" class=\"checkbox ml-4\" id=\"completeTaskListPri'.\$taskList->id.'\" name=\"completeTaskListPri'.\$taskList->id.'\"';
                    if(\$taskList->completed){
                        echo ' checked>';
                        echo \"<style type='text/css'> .priTaskList\".\$taskList->id.\" { text-decoration: line-through;} </style>\";
                    }
                    else{
                        echo '>';
                    }
                    echo '<kbd class=\"ml-1\" for=\"completeTaskListPri'.\$taskList->id.'\">Compléter</kdb>';
                    echo \"</div>\";
                }
                echo \"</div>\";
                echo \"</div>\";
                echo \"<div class='text-center'>\";
                echo \"<button type=\\\"submit\\\" class=\\\"text-center h5 btn-primary p-1 rounded btn\\\">Valider complétion de liste Privée</button>\";
                echo \"<input type=\\\"hidden\\\" name=\\\"action\\\" value=\\\"valPrivate\\\">\";
                echo \"<input type=\\\"hidden\\\" name=\\\"pp\\\" value=\\\"\$pageP\\\">\";
                echo \"</div>\";
                echo \"</form>\";
            }
            else{
                echo \"<h3 align='center'>Pas de listes privées</h3>\";
            }


            //TODO : je veux ca https://vincentgarreau.com/particles.js/
        }-->
        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">

              <!--  if(isset(\$totTaskListP) && \$totTaskListP != 0) {
                    if(isset(\$pageP)) {
                        if (\$pageP != 1) {
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=1&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'>1  </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$pageP - 1).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'> < </a></li>\";
                        }
                        echo \"<span class='page-link'>\$pageP</span>\";
                        if (\$pageP != \$nbPageP) {
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$pageP + 1).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'> > </a></li>\";
                            echo \"<li class='page-item'><a href=\\\"index.php?pp=\".(\$nbPageP).\"&p=\".(isset(\$page)?\$page:1).\"#privateTaskListDisplay\\\" class='page-link'>\".\$nbPageP.\"</a></li>\";
                        }
                    }
                }
                -->
            </nav>
        </div>
        <!--
        if(isset(\$u)){
            echo '<div class=\"text-center mt-5\">';
            echo '<a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAddPrivate\">Ajout d\\'une liste</a>';
            echo '</div>';
        }
        -->

    </div>

    <!--            <script type=\"text/javascript\" src=\"view/js/particles.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/app.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/particle1.js\"></script> -->
    <script type=\"text/javascript\" src=\"view/js/particle2.js\"></script>
</main>
</body>
</html>
", "firstTemplate.php", "C:\\wamp64\\www\\to_do_list_php\\view\\template\\firstTemplate.php");
    }
}
