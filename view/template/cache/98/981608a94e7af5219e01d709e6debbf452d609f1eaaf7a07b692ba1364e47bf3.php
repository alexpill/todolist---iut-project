<?php

/* firstTemplate.php */
class __TwigTemplate_74eecfc8099e83806db1102a5266a023a7edd7e57c8923b2c037689c99ccebc2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title> TellMeWhatTODO. </title>
    <link rel=\"stylesheet\" href=\"view/css/bootstrap.min.css\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"view/css/global.css\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Megrim\" rel=\"stylesheet\">
</head>
<body>
<main>
    <canvas id=\"canvas_boom\"></canvas>
    <nav class=\"navbar navbar-light\">
        <a class=\"navbar-brand\" href=\"index.php#\"><p class=\"display-3\" id=\"todo_title\">TellMeWhatTODO</p></a>
            <div class=\"\">
                ";
        // line 17
        if (( !(isset($context["a"]) || array_key_exists("a", $context)) &&  !(isset($context["u"]) || array_key_exists("u", $context)))) {
            // line 18
            echo "                    <div class='container align-content-start'>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded mr-1\" href=\"./index.php?action=viewConnectionAdmin\">Connection Admin</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded ml-1\" href=\"./index.php?action=viewConnectionUser\">Connection User</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded \" href=\"./index.php?action=viewInscriptionUser\">Inscription</a>
                    </div>
                    </div>
                ";
        }
        // line 30
        echo "                ";
        if ((isset($context["a"]) || array_key_exists("a", $context))) {
            // line 31
            echo "                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionAdmin\">Déconnexion ";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["a"] ?? null), "role", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["a"] ?? null), "login", array()), "html", null, true);
            echo "</a>
                    </div>
                ";
        }
        // line 35
        echo "
                ";
        // line 36
        if ((isset($context["u"]) || array_key_exists("u", $context))) {
            // line 37
            echo "                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionUser\">Déconnexion ";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "role", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "login", array()), "html", null, true);
            echo "</a>
                    </div>
                ";
        }
        // line 41
        echo "            </div>
    </nav>

    <div id=\"main\">
        ";
        // line 45
        if ((isset($context["taskLists"]) || array_key_exists("taskLists", $context))) {
            // line 46
            echo "            <h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>
            ";
            // line 47
            if (((isset($context["totTaskList"]) || array_key_exists("totTaskList", $context)) && (($context["totTaskList"] ?? null) != 0))) {
                // line 48
                echo "                <form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">
                    <div class='container-fluid card-body w-auto'>
                        <div class='row'>
                ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["taskLists"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["taskList"]) {
                    // line 52
                    echo "                            <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                                <a href='index.php?action=viewList&id=";
                    // line 53
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "' class='text-center text-light pubTaskList";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'><h3>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "title", array()), "html", null, true);
                    echo "</h3></a>
                                <h4 class='text-center pubTaskList";
                    // line 54
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "comment", array()), "html", null, true);
                    echo "</h4>
                                <a href=\"./index.php?action=delPublic&id=";
                    // line 55
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                                ";
                    // line 56
                    if (twig_get_attribute($this->env, $this->source, $context["taskList"], "completed", array())) {
                        // line 57
                        echo "                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" name=\"completeTaskList";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" checked>
                                <style type='text/css'> .pubTaskList";
                        // line 58
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo " { text-decoration: line-through;} </style>
                                ";
                    } else {
                        // line 60
                        echo "                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" name=\"completeTaskList";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\">
                                ";
                    }
                    // line 62
                    echo "                                <kbd class='ml-1' for=\"completeTaskList";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\">Compléter</kbd>
                            </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['taskList'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "                        </div>
                    </div>
                    <div class='text-center'>
                        <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste publique</button>
                        <input type=\"hidden\" name=\"action\" value=\"valPublic\">
                        <input type=\"hidden\" name=\"p\" value=";
                // line 70
                echo twig_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
                echo "\">
                    </div>
                </form>
        ";
            } else {
                // line 74
                echo "        <h3 align='center'>Pas de liste publiques</h3>
        ";
            }
            // line 76
            echo "        ";
        }
        // line 77
        echo "
        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
            ";
        // line 80
        if (((isset($context["totTaskList"]) || array_key_exists("totTaskList", $context)) && (($context["totTaskList"] ?? null) != 0))) {
            // line 81
            echo "                ";
            if ((isset($context["page"]) || array_key_exists("page", $context))) {
                // line 82
                echo "                    ";
                if ((($context["page"] ?? null) != 1)) {
                    // line 83
                    echo "                <li class='page-item'><a href=\"index.php?p=1";
                    echo twig_escape_filter($this->env, (((isset($context["pageP"]) || array_key_exists("pageP", $context))) ? (("&pp=" . ($context["pageP"] ?? null))) : ("")), "html", null, true);
                    echo "\" class='page-link'>1  </a></li>
                <li class='page-item'><a href=\"index.php?p=";
                    // line 84
                    echo twig_escape_filter($this->env, (($context["page"] ?? null) - 1), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["pageP"]) || array_key_exists("pageP", $context))) ? (("&pp=" . ($context["pageP"] ?? null))) : ("")), "html", null, true);
                    echo "\" class='page-link'> < </a></li>
                    ";
                }
                // line 86
                echo "                <span class='page-link'>";
                echo twig_escape_filter($this->env, ($context["page"] ?? null), "html", null, true);
                echo "</span>
                    ";
                // line 87
                if ((($context["page"] ?? null) != ($context["nbPage"] ?? null))) {
                    // line 88
                    echo "                <li class='page-item'><a href=\"index.php?p=";
                    echo twig_escape_filter($this->env, (($context["page"] ?? null) + 1), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["pageP"]) || array_key_exists("pageP", $context))) ? (("&pp=" . ($context["pageP"] ?? null))) : ("")), "html", null, true);
                    echo "\" class='page-link'> > </a></li>
                <li class='page-item'><a href=\"index.php?p=";
                    // line 89
                    echo twig_escape_filter($this->env, ($context["nbPage"] ?? null), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["pageP"]) || array_key_exists("pageP", $context))) ? (("&pp=" . ($context["pageP"] ?? null))) : ("")), "html", null, true);
                    echo "\" class='page-link'>";
                    echo twig_escape_filter($this->env, ($context["nbPage"] ?? null), "html", null, true);
                    echo "</a></li>
                    ";
                }
                // line 91
                echo "                ";
            }
            // line 92
            echo "            ";
        }
        // line 93
        echo "            </nav>
        </div>
        ";
        // line 95
        if ((isset($context["taskLists"]) || array_key_exists("taskLists", $context))) {
            // line 96
            echo "        <div class=\"text-center mt-5\">
            <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAdd\">Ajout d'une liste</a>
        </div>
        ";
        }
        // line 100
        echo "
        ";
        // line 101
        if (((isset($context["taskListsP"]) || array_key_exists("taskListsP", $context)) && (isset($context["u"]) || array_key_exists("u", $context)))) {
            // line 102
            echo "        <h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["u"] ?? null), "login", array()), "html", null, true);
            echo " </h2>
            ";
            // line 103
            if (((isset($context["totTaskListP"]) || array_key_exists("totTaskListP", $context)) && (($context["totTaskListP"] ?? null) != 0))) {
                // line 104
                echo "        <form action=\"index.php\" method=\"post\" name=\"formTaskListPrivate\">
            <div class='container-fluid card-body w-auto'>
                <div class='row'>
                    ";
                // line 107
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["taskListsP"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["taskList"]) {
                    // line 108
                    echo "                    <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                        <a href='index.php?action=viewListPrivate&id=";
                    // line 109
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "' class='text-center text-light priTaskList";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'><h3>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "title", array()), "html", null, true);
                    echo "</h3></a>
                        <h4 class='text-center priTaskList";
                    // line 110
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "'>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "comment", array()), "html", null, true);
                    echo "</h4>
                        <a href=\"./index.php?action=delPrivate&id=";
                    // line 111
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                        ";
                    // line 112
                    if (twig_get_attribute($this->env, $this->source, $context["taskList"], "completed", array())) {
                        // line 113
                        echo "                        <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskListPri";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" name=\"completeTaskListPri";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" checked>
                        <style type='text/css'> .priTaskList";
                        // line 114
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo " { text-decoration: line-through;} </style>
                        ";
                    } else {
                        // line 116
                        echo "                        <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskListPri";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\" name=\"completeTaskListPri";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                        echo "\">
                        ";
                    }
                    // line 118
                    echo "                        <kbd class=\"ml-1\" for=\"completeTaskListPri";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["taskList"], "id", array()), "html", null, true);
                    echo "\">Compléter</kbd>
                    </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['taskList'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 121
                echo "                </div>
            </div>
            <div class='text-center'>
                <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste Privée</button>
                <input type=\"hidden\" name=\"action\" value=\"valPrivate\">
                <input type=\"hidden\" name=\"pp\" value=\"";
                // line 126
                echo twig_escape_filter($this->env, ($context["pageP"] ?? null), "html", null, true);
                echo "\">
            </div>
        </form>
            ";
            } else {
                // line 130
                echo "        <h3 align='center'>Pas de listes privées</h3>
            ";
            }
            // line 132
            echo "
        ";
        }
        // line 134
        echo "
        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
                ";
        // line 137
        if (((isset($context["totTaskListP"]) || array_key_exists("totTaskListP", $context)) && (($context["totTaskListP"] ?? null) != 0))) {
            // line 138
            echo "                ";
            if ((isset($context["pageP"]) || array_key_exists("pageP", $context))) {
                // line 139
                echo "                ";
                if ((($context["pageP"] ?? null) != 1)) {
                    // line 140
                    echo "                <li class='page-item'><a href=\"index.php?pp=1";
                    echo twig_escape_filter($this->env, (((isset($context["page"]) || array_key_exists("page", $context))) ? (("&p=" . ($context["page"] ?? null))) : ("&p=1")), "html", null, true);
                    echo "#privateTaskListDisplay\" class='page-link'>1  </a></li>
                <li class='page-item'><a href=\"index.php?pp=";
                    // line 141
                    echo twig_escape_filter($this->env, (($context["pageP"] ?? null) - 1), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["page"]) || array_key_exists("page", $context))) ? (("&p=" . ($context["page"] ?? null))) : ("&p=1")), "html", null, true);
                    echo "#privateTaskListDisplay\" class='page-link'> < </a></li>
                ";
                }
                // line 143
                echo "                <span class='page-link'>";
                echo twig_escape_filter($this->env, ($context["pageP"] ?? null), "html", null, true);
                echo "</span>
                ";
                // line 144
                if ((($context["pageP"] ?? null) != ($context["nbPageP"] ?? null))) {
                    // line 145
                    echo "                <li class='page-item'><a href=\"index.php?pp=";
                    echo twig_escape_filter($this->env, (($context["pageP"] ?? null) + 1), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["page"]) || array_key_exists("page", $context))) ? (("&p=" . ($context["page"] ?? null))) : ("&p=1")), "html", null, true);
                    echo "#privateTaskListDisplay\" class='page-link'> > </a></li>
                <li class='page-item'><a href=\"index.php?pp=";
                    // line 146
                    echo twig_escape_filter($this->env, ($context["nbPageP"] ?? null), "html", null, true);
                    echo twig_escape_filter($this->env, (((isset($context["page"]) || array_key_exists("page", $context))) ? (("&p=" . ($context["page"] ?? null))) : ("&p=1")), "html", null, true);
                    echo "#privateTaskListDisplay\" class='page-link'>";
                    echo twig_escape_filter($this->env, ($context["nbPageP"] ?? null), "html", null, true);
                    echo "</a></li>
                ";
                }
                // line 148
                echo "                ";
            }
            // line 149
            echo "                ";
        }
        // line 150
        echo "
            </nav>
        </div>
        ";
        // line 153
        if ((isset($context["u"]) || array_key_exists("u", $context))) {
            // line 154
            echo "            <div class=\"text-center mt-5\">
                <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAddPrivate\">Ajout d'une liste</a>
            </div>
        ";
        }
        // line 158
        echo "
        ";
        // line 159
        if (((isset($context["a"]) || array_key_exists("a", $context)) && (isset($context["users"]) || array_key_exists("users", $context)))) {
            // line 160
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 161
                echo "        <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
            <h3 class='text-center text-light'>";
                // line 162
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "login", array()), "html", null, true);
                echo "</h3>
            <a href=\"./index.php?action=delUser&id=";
                // line 163
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", array()), "html", null, true);
                echo "\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
        </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 166
            echo "        ";
        }
        // line 167
        echo "
    </div>

    <!--            <script type=\"text/javascript\" src=\"view/js/particles.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/app.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/particle1.js\"></script> -->
    <script type=\"text/javascript\" src=\"view/js/particle2.js\"></script>
</main>
<footer>
    <label for=\"nbListByPage\">List on Page : </label>
    <select name=\"nbListByPage\" form=\"footerForm\">
        <option value=\"2\">2</option>
        <option value=\"4\" selected>4</option>
        <option value=\"6\">6</option>
        <option value=\"8\">8</option>
        <option value=\"10\">10</option>
    </select>
    <label for=\"nbTaskByPage\">Task on Page : </label>
    <select name=\"nbTaskByPage\" form=\"footerForm\">
        <option value=\"2\">2</option>
        <option value=\"4\">4</option>
        <option value=\"6\" selected>6</option>
        <option value=\"8\">8</option>
        <option value=\"10\">10</option>
    </select>
    <form action=\"index.php\" id=\"footerForm\" method=\"post\">
        <input type=\"submit\" value=\"Changer\"/>
        <input type=\"hidden\" name=\"action\" value=\"changeNbByPage\"/>
    </form>
</footer>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "firstTemplate.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 167,  432 => 166,  423 => 163,  419 => 162,  416 => 161,  411 => 160,  409 => 159,  406 => 158,  400 => 154,  398 => 153,  393 => 150,  390 => 149,  387 => 148,  379 => 146,  373 => 145,  371 => 144,  366 => 143,  360 => 141,  355 => 140,  352 => 139,  349 => 138,  347 => 137,  342 => 134,  338 => 132,  334 => 130,  327 => 126,  320 => 121,  310 => 118,  302 => 116,  297 => 114,  290 => 113,  288 => 112,  284 => 111,  278 => 110,  270 => 109,  267 => 108,  263 => 107,  258 => 104,  256 => 103,  251 => 102,  249 => 101,  246 => 100,  240 => 96,  238 => 95,  234 => 93,  231 => 92,  228 => 91,  220 => 89,  214 => 88,  212 => 87,  207 => 86,  201 => 84,  196 => 83,  193 => 82,  190 => 81,  188 => 80,  183 => 77,  180 => 76,  176 => 74,  169 => 70,  162 => 65,  152 => 62,  144 => 60,  139 => 58,  132 => 57,  130 => 56,  126 => 55,  120 => 54,  112 => 53,  109 => 52,  105 => 51,  100 => 48,  98 => 47,  95 => 46,  93 => 45,  87 => 41,  79 => 38,  76 => 37,  74 => 36,  71 => 35,  63 => 32,  60 => 31,  57 => 30,  43 => 18,  41 => 17,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title> TellMeWhatTODO. </title>
    <link rel=\"stylesheet\" href=\"view/css/bootstrap.min.css\" type=\"text/css\">
    <link rel=\"stylesheet\" href=\"view/css/global.css\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Megrim\" rel=\"stylesheet\">
</head>
<body>
<main>
    <canvas id=\"canvas_boom\"></canvas>
    <nav class=\"navbar navbar-light\">
        <a class=\"navbar-brand\" href=\"index.php#\"><p class=\"display-3\" id=\"todo_title\">TellMeWhatTODO</p></a>
            <div class=\"\">
                {% if a is not defined and u is not defined %}
                    <div class='container align-content-start'>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded mr-1\" href=\"./index.php?action=viewConnectionAdmin\">Connection Admin</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded ml-1\" href=\"./index.php?action=viewConnectionUser\">Connection User</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class=\"text-center h5 btn-secondary p-1 rounded \" href=\"./index.php?action=viewInscriptionUser\">Inscription</a>
                    </div>
                    </div>
                {% endif %}
                {% if a is defined %}
                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionAdmin\">Déconnexion {{ a.role }} {{ a.login }}</a>
                    </div>
                {% endif %}

                {% if u is defined %}
                    <div>
                        <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=deconnexionUser\">Déconnexion {{ u.role }} {{ u.login }}</a>
                    </div>
                {% endif %}
            </div>
    </nav>

    <div id=\"main\">
        {% if taskLists is defined %}
            <h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>
            {% if totTaskList is defined and totTaskList != 0 %}
                <form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">
                    <div class='container-fluid card-body w-auto'>
                        <div class='row'>
                {% for taskList in taskLists %}
                            <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                                <a href='index.php?action=viewList&id={{taskList.id}}' class='text-center text-light pubTaskList{{taskList.id}}'><h3>{{taskList.title}}</h3></a>
                                <h4 class='text-center pubTaskList{{taskList.id}}'>{{taskList.comment}}</h4>
                                <a href=\"./index.php?action=delPublic&id={{taskList.id}}\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                                {% if taskList.completed %}
                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList{{taskList.id}}\" name=\"completeTaskList{{taskList.id}}\" checked>
                                <style type='text/css'> .pubTaskList{{taskList.id}} { text-decoration: line-through;} </style>
                                {% else %}
                                <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskList{{taskList.id}}\" name=\"completeTaskList{{taskList.id}}\">
                                {% endif %}
                                <kbd class='ml-1' for=\"completeTaskList{{taskList.id}}\">Compléter</kbd>
                            </div>
                {% endfor %}
                        </div>
                    </div>
                    <div class='text-center'>
                        <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste publique</button>
                        <input type=\"hidden\" name=\"action\" value=\"valPublic\">
                        <input type=\"hidden\" name=\"p\" value={{page}}\">
                    </div>
                </form>
        {% else %}
        <h3 align='center'>Pas de liste publiques</h3>
        {% endif %}
        {% endif %}

        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
            {% if totTaskList is defined and totTaskList != 0 %}
                {% if page is defined %}
                    {% if page != 1 %}
                <li class='page-item'><a href=\"index.php?p=1{{ ( pageP is defined ) ? \"&pp=#{pageP}\" : \"\" }}\" class='page-link'>1  </a></li>
                <li class='page-item'><a href=\"index.php?p={{ page - 1 }}{{ ( pageP is defined ) ? \"&pp=#{pageP}\" : \"\" }}\" class='page-link'> < </a></li>
                    {% endif %}
                <span class='page-link'>{{page}}</span>
                    {% if page != nbPage %}
                <li class='page-item'><a href=\"index.php?p={{ page + 1 }}{{ ( pageP is defined ) ? \"&pp=#{pageP}\" : \"\" }}\" class='page-link'> > </a></li>
                <li class='page-item'><a href=\"index.php?p={{ nbPage }}{{ ( pageP is defined ) ? \"&pp=#{pageP}\" : \"\" }}\" class='page-link'>{{nbPage}}</a></li>
                    {% endif %}
                {% endif %}
            {% endif %}
            </nav>
        </div>
        {% if taskLists is defined %}
        <div class=\"text-center mt-5\">
            <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAdd\">Ajout d'une liste</a>
        </div>
        {% endif %}

        {% if taskListsP is defined and u is defined %}
        <h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de {{u.login}} </h2>
            {% if totTaskListP is defined and totTaskListP != 0 %}
        <form action=\"index.php\" method=\"post\" name=\"formTaskListPrivate\">
            <div class='container-fluid card-body w-auto'>
                <div class='row'>
                    {% for taskList in taskListsP %}
                    <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                        <a href='index.php?action=viewListPrivate&id={{taskList.id}}' class='text-center text-light priTaskList{{taskList.id}}'><h3>{{taskList.title}}</h3></a>
                        <h4 class='text-center priTaskList{{taskList.id}}'>{{taskList.comment}}</h4>
                        <a href=\"./index.php?action=delPrivate&id={{taskList.id}}\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                        {% if taskList.completed %}
                        <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskListPri{{taskList.id}}\" name=\"completeTaskListPri{{taskList.id}}\" checked>
                        <style type='text/css'> .priTaskList{{taskList.id}} { text-decoration: line-through;} </style>
                        {% else %}
                        <input type=\"checkbox\" class=\"checkbox ml-4 align-content-center\" id=\"completeTaskListPri{{taskList.id}}\" name=\"completeTaskListPri{{taskList.id}}\">
                        {% endif %}
                        <kbd class=\"ml-1\" for=\"completeTaskListPri{{taskList.id}}\">Compléter</kbd>
                    </div>
                    {% endfor %}
                </div>
            </div>
            <div class='text-center'>
                <button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider complétion de liste Privée</button>
                <input type=\"hidden\" name=\"action\" value=\"valPrivate\">
                <input type=\"hidden\" name=\"pp\" value=\"{{pageP}}\">
            </div>
        </form>
            {% else %}
        <h3 align='center'>Pas de listes privées</h3>
            {% endif %}

        {% endif %}

        <div align=\"center\" class=\"container\">
            <nav class=\"pagination justify-content-center w-100\">
                {% if totTaskListP is defined and totTaskListP != 0 %}
                {% if pageP is defined %}
                {% if pageP != 1 %}
                <li class='page-item'><a href=\"index.php?pp=1{{ ( page is defined ) ? \"&p=#{page}\" : \"&p=1\" }}#privateTaskListDisplay\" class='page-link'>1  </a></li>
                <li class='page-item'><a href=\"index.php?pp={{ pageP - 1 }}{{ ( page is defined ) ? \"&p=#{page}\" : \"&p=1\" }}#privateTaskListDisplay\" class='page-link'> < </a></li>
                {% endif %}
                <span class='page-link'>{{pageP}}</span>
                {% if pageP != nbPageP %}
                <li class='page-item'><a href=\"index.php?pp={{ pageP + 1 }}{{ ( page is defined ) ? \"&p=#{page}\" : \"&p=1\" }}#privateTaskListDisplay\" class='page-link'> > </a></li>
                <li class='page-item'><a href=\"index.php?pp={{ nbPageP }}{{ ( page is defined ) ? \"&p=#{page}\" : \"&p=1\" }}#privateTaskListDisplay\" class='page-link'>{{nbPageP}}</a></li>
                {% endif %}
                {% endif %}
                {% endif %}

            </nav>
        </div>
        {% if u is defined %}
            <div class=\"text-center mt-5\">
                <a class=\"text-center h5 btn-secondary p-1 rounded\" href=\"./index.php?action=viewAddPrivate\">Ajout d'une liste</a>
            </div>
        {% endif %}

        {% if a is defined and users is defined %}
            {% for user in users %}
        <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
            <h3 class='text-center text-light'>{{user.login}}</h3>
            <a href=\"./index.php?action=delUser&id={{user.id}}\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
        </div>
            {% endfor %}
        {% endif %}

    </div>

    <!--            <script type=\"text/javascript\" src=\"view/js/particles.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/app.js\"></script>-->
    <!--            <script type=\"text/javascript\" src=\"view/js/particle1.js\"></script> -->
    <script type=\"text/javascript\" src=\"view/js/particle2.js\"></script>
</main>
<footer>
    <label for=\"nbListByPage\">List on Page : </label>
    <select name=\"nbListByPage\" form=\"footerForm\">
        <option value=\"2\">2</option>
        <option value=\"4\" selected>4</option>
        <option value=\"6\">6</option>
        <option value=\"8\">8</option>
        <option value=\"10\">10</option>
    </select>
    <label for=\"nbTaskByPage\">Task on Page : </label>
    <select name=\"nbTaskByPage\" form=\"footerForm\">
        <option value=\"2\">2</option>
        <option value=\"4\">4</option>
        <option value=\"6\" selected>6</option>
        <option value=\"8\">8</option>
        <option value=\"10\">10</option>
    </select>
    <form action=\"index.php\" id=\"footerForm\" method=\"post\">
        <input type=\"submit\" value=\"Changer\"/>
        <input type=\"hidden\" name=\"action\" value=\"changeNbByPage\"/>
    </form>
</footer>
</body>
</html>
", "firstTemplate.php", "C:\\wamp64\\www\\to_do_list_php\\view\\template\\firstTemplate.php");
    }
}
