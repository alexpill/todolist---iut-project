<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> TellMeWhatTODO. </title>
    <link rel="stylesheet" href="view/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="view/css/global.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Megrim" rel="stylesheet">
</head>
<body>
<main>
    <canvas id="canvas_boom"></canvas>
    <nav class="navbar navbar-light">
        <a class="navbar-brand" href="index.php#"><p class="display-3" id="todo_title">TellMeWhatTODO</p></a>
            <div class="">
                {% if a is not defined and u is not defined %}
                    <div class='container align-content-start'>
                    <div class='row justify-content-end'>
                    <a class="text-center h5 btn-secondary p-1 rounded mr-1" href="./index.php?action=viewConnectionAdmin">Connection Admin</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class="text-center h5 btn-secondary p-1 rounded ml-1" href="./index.php?action=viewConnectionUser">Connection User</a>
                    </div>
                    <div class='row justify-content-end'>
                    <a class="text-center h5 btn-secondary p-1 rounded " href="./index.php?action=viewInscriptionUser">Inscription</a>
                    </div>
                    </div>
                {% endif %}
                {% if a is defined %}
                    <div>
                        <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=deconnexionAdmin">Déconnexion {{ a.role }} {{ a.login }}</a>
                    </div>
                {% endif %}

                {% if u is defined %}
                    <div>
                        <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=deconnexionUser">Déconnexion {{ u.role }} {{ u.login }}</a>
                    </div>
                {% endif %}
            </div>
    </nav>

    <div id="main">
        {% if taskLists is defined %}
            <h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Publiques </h2>
            {% if totTaskList is defined and totTaskList != 0 %}
                <form action="index.php" method="post" name="formTaskListPublic">
                    <div class='container-fluid card-body w-auto'>
                        <div class='row'>
                {% for taskList in taskLists %}
                            <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                                <a href='index.php?action=viewList&id={{taskList.id}}' class='text-center text-light pubTaskList{{taskList.id}}'><h3>{{taskList.title}}</h3></a>
                                <h4 class='text-center pubTaskList{{taskList.id}}'>{{taskList.comment}}</h4>
                                <a href="./index.php?action=delPublic&id={{taskList.id}}"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                                {% if taskList.completed %}
                                <input type="checkbox" class="checkbox ml-4 align-content-center" id="completeTaskList{{taskList.id}}" name="completeTaskList{{taskList.id}}" checked>
                                <style type='text/css'> .pubTaskList{{taskList.id}} { text-decoration: line-through;} </style>
                                {% else %}
                                <input type="checkbox" class="checkbox ml-4 align-content-center" id="completeTaskList{{taskList.id}}" name="completeTaskList{{taskList.id}}">
                                {% endif %}
                                <kbd class='ml-1' for="completeTaskList{{taskList.id}}">Compléter</kbd>
                            </div>
                {% endfor %}
                        </div>
                    </div>
                    <div class='text-center'>
                        <button type="submit" class="text-center h5 btn-primary p-1 rounded btn">Valider complétion de liste publique</button>
                        <input type="hidden" name="action" value="valPublic">
                        <input type="hidden" name="p" value={{page}}">
                    </div>
                </form>
        {% else %}
        <h3 align='center'>Pas de liste publiques</h3>
        {% endif %}
        {% endif %}

        <div align="center" class="container">
            <nav class="pagination justify-content-center w-100">
            {% if totTaskList is defined and totTaskList != 0 %}
                {% if page is defined %}
                    {% if page != 1 %}
                <li class='page-item'><a href="index.php?p=1{{ ( pageP is defined ) ? "&pp=#{pageP}" : "" }}" class='page-link'>1  </a></li>
                <li class='page-item'><a href="index.php?p={{ page - 1 }}{{ ( pageP is defined ) ? "&pp=#{pageP}" : "" }}" class='page-link'> < </a></li>
                    {% endif %}
                <span class='page-link'>{{page}}</span>
                    {% if page != nbPage %}
                <li class='page-item'><a href="index.php?p={{ page + 1 }}{{ ( pageP is defined ) ? "&pp=#{pageP}" : "" }}" class='page-link'> > </a></li>
                <li class='page-item'><a href="index.php?p={{ nbPage }}{{ ( pageP is defined ) ? "&pp=#{pageP}" : "" }}" class='page-link'>{{nbPage}}</a></li>
                    {% endif %}
                {% endif %}
            {% endif %}
            </nav>
        </div>
        {% if taskLists is defined %}
        <div class="text-center mt-5">
            <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=viewAdd">Ajout d'une liste</a>
        </div>
        {% endif %}

        {% if taskListsP is defined and u is defined %}
        <h2 id='privateTaskListDisplay' class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Listes Privées de {{u.login}} </h2>
            {% if totTaskListP is defined and totTaskListP != 0 %}
        <form action="index.php" method="post" name="formTaskListPrivate">
            <div class='container-fluid card-body w-auto'>
                <div class='row'>
                    {% for taskList in taskListsP %}
                    <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
                        <a href='index.php?action=viewListPrivate&id={{taskList.id}}' class='text-center text-light priTaskList{{taskList.id}}'><h3>{{taskList.title}}</h3></a>
                        <h4 class='text-center priTaskList{{taskList.id}}'>{{taskList.comment}}</h4>
                        <a href="./index.php?action=delPrivate&id={{taskList.id}}"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
                        {% if taskList.completed %}
                        <input type="checkbox" class="checkbox ml-4 align-content-center" id="completeTaskListPri{{taskList.id}}" name="completeTaskListPri{{taskList.id}}" checked>
                        <style type='text/css'> .priTaskList{{taskList.id}} { text-decoration: line-through;} </style>
                        {% else %}
                        <input type="checkbox" class="checkbox ml-4 align-content-center" id="completeTaskListPri{{taskList.id}}" name="completeTaskListPri{{taskList.id}}">
                        {% endif %}
                        <kbd class="ml-1" for="completeTaskListPri{{taskList.id}}">Compléter</kbd>
                    </div>
                    {% endfor %}
                </div>
            </div>
            <div class='text-center'>
                <button type="submit" class="text-center h5 btn-primary p-1 rounded btn">Valider complétion de liste Privée</button>
                <input type="hidden" name="action" value="valPrivate">
                <input type="hidden" name="pp" value="{{pageP}}">
            </div>
        </form>
            {% else %}
        <h3 align='center'>Pas de listes privées</h3>
            {% endif %}

        {% endif %}

        <div align="center" class="container">
            <nav class="pagination justify-content-center w-100">
                {% if totTaskListP is defined and totTaskListP != 0 %}
                {% if pageP is defined %}
                {% if pageP != 1 %}
                <li class='page-item'><a href="index.php?pp=1{{ ( page is defined ) ? "&p=#{page}" : "&p=1" }}#privateTaskListDisplay" class='page-link'>1  </a></li>
                <li class='page-item'><a href="index.php?pp={{ pageP - 1 }}{{ ( page is defined ) ? "&p=#{page}" : "&p=1" }}#privateTaskListDisplay" class='page-link'> < </a></li>
                {% endif %}
                <span class='page-link'>{{pageP}}</span>
                {% if pageP != nbPageP %}
                <li class='page-item'><a href="index.php?pp={{ pageP + 1 }}{{ ( page is defined ) ? "&p=#{page}" : "&p=1" }}#privateTaskListDisplay" class='page-link'> > </a></li>
                <li class='page-item'><a href="index.php?pp={{ nbPageP }}{{ ( page is defined ) ? "&p=#{page}" : "&p=1" }}#privateTaskListDisplay" class='page-link'>{{nbPageP}}</a></li>
                {% endif %}
                {% endif %}
                {% endif %}

            </nav>
        </div>
        {% if u is defined %}
            <div class="text-center mt-5">
                <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=viewAddPrivate">Ajout d'une liste</a>
            </div>
        {% endif %}

        {% if a is defined and users is defined %}
            {% for user in users %}
        <div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>
            <h3 class='text-center text-light'>{{user.login}}</h3>
            <a href="./index.php?action=delUser&id={{user.id}}"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>
        </div>
            {% endfor %}
        {% endif %}

    </div>

    <!--            <script type="text/javascript" src="view/js/particles.js"></script>-->
    <!--            <script type="text/javascript" src="view/js/app.js"></script>-->
    <!--            <script type="text/javascript" src="view/js/particle1.js"></script> -->
    <script type="text/javascript" src="view/js/particle2.js"></script>
</main>
<footer>
    <label for="nbListByPage">List on Page : </label>
    <select name="nbListByPage" form="footerForm">
        <option value="2">2</option>
        <option value="4" selected>4</option>
        <option value="6">6</option>
        <option value="8">8</option>
        <option value="10">10</option>
    </select>
    <label for="nbTaskByPage">Task on Page : </label>
    <select name="nbTaskByPage" form="footerForm">
        <option value="2">2</option>
        <option value="4">4</option>
        <option value="6" selected>6</option>
        <option value="8">8</option>
        <option value="10">10</option>
    </select>
    <form action="index.php" id="footerForm" method="post">
        <input type="submit" value="Changer"/>
        <input type="hidden" name="action" value="changeNbByPage"/>
    </form>
</footer>
</body>
</html>
