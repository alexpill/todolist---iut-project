<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> TellMeWhatTODO. </title>
    <link rel="stylesheet" href="view/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="view/css/global.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Megrim" rel="stylesheet">
</head>
<body>
<main>
    <a class="text-center h5 btn-secondary p-1 rounded" style="position: absolute;right: 0%;z-index: 9999" href="index.php">Retour</a>
    <canvas id="canvas_boom"></canvas>
    <nav class="navbar navbar-light">
        <a class="navbar-brand" href="index.php#"><p class="display-3" id="todo_title">TellMeWhatTODO</p></a>
    </nav>
    <div id="main" class="jumbotron jumbotron-fluid mt-2">
    <h1 class="alert-warning text-center">Inscription</h1>
    <?php
        if(isset($err)){
            foreach ($err as $er){
                echo "<h2 class=\"alert-warning text-center\">$er</h2>";
            }
        }
    ?>
    <form class="form-group" action="index.php" method="post" name="formInscriptionUser">

        <label id="Titre">Login:</label>
        <input  class="form-control" id="Titre" type="text" name="login"></br>

        <label>Mot de passe:</label>
        <input type="password" class="form-control" id="Password" name="mdp" placeholder="Choisir un mot de passe"></br>
        <input type="password" class="form-control" id="Password" name="mdp_verify" placeholder="Confirmer le mot de passe"></br>
        <div class="text-center mb-2">
            <button type="submit" class="btn-primary align-self-center mb-3">Connection</button>
            <input type="hidden" name="action" value="inscriptionUser">
        </div>
    </form>
    <script type="text/javascript" src="view/js/particle2.js"></script>
    </div>
</main>
</body>
</html>
