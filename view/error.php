<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> TellMeWhatTODO. </title>
    <link rel="stylesheet" href="view/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="view/css/global.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Megrim" rel="stylesheet">
</head>
<body>
<main>
    <a class="text-center h5 btn-secondary p-1 rounded" style="position: absolute;right: 0%;z-index: 9999" href="index.php">Retour</a>
    <canvas id="canvas_boom"></canvas>
    <nav class="navbar navbar-light">
        <a class="navbar-brand" href="index.php#"><p class="display-3" id="todo_title">TellMeWhatTODO</p></a>
    </nav>
    <div id="main" style="background: #b9bbbe">
    <?php
    if(isset($viewError))
    {
        foreach ($viewError as $error)
        {
            echo "$error";
        }
    }
    ?>
    </div>

    <!--            <script type="text/javascript" src="view/js/particles.js"></script>-->
    <!--            <script type="text/javascript" src="view/js/app.js"></script>-->
    <!--            <script type="text/javascript" src="view/js/particle1.js"></script> -->
    <script type="text/javascript" src="view/js/particle2.js"></script>
</main>
</body>
</html>
