<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> TellMeWhatTODO. </title>
    <link rel="stylesheet" href="view/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="view/css/global.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Megrim" rel="stylesheet">

</head>
<body>
<main>
    <a class="text-center h5 btn-secondary p-1 rounded" style="position: absolute;right: 0%;z-index: 9999" href="index.php">Retour</a>
    <canvas id="canvas_boom"></canvas>
    <nav class="navbar navbar-light">
        <a class="navbar-brand" href="index.php#"><p class="display-3" id="todo_title">TellMeWhatTODO</p></a>
    </nav>
    <div id="main" class="jumbotron jumbotron-fluid mt-2">
    <?php
    if(isset($tasks) && count($tasks) > 0 & isset($list)){

            echo "<h2 class='jumbotron jumbotron-fluid text-center mt-4 p-auto' align='center'> Tâche de $list->title</h2>";
            echo "<form action=\"index.php\" method=\"post\" name=\"formTaskListPublic\">";
            echo "<div class='container-fluid card-body w-auto'>";
            echo "<div class='row'>";
            foreach ($tasks as $task)
            {
                echo "<div class='bg-secondary col-sm-6 border w-100 mb-2 rounded' align='center'>";
                echo "<h3 class='text-center text-light task".$task->id."'>" . $task->title . "</h3>";
                echo "<h4 class='text-center task".$task->id."'>" . $task->comment . "</h4>";
                if(!$isPrivate){
                    echo "<a href=\"./index.php?action=delTaskPublic&idT=".$task->id."&id=".$list->id."\" class='btn-dark btn mb-2 rounded-circle font-weight-bold'>X</a>";

                }else{
                    echo "<a href=\"./index.php?action=delTaskPrivate&idT=".$task->id."&id=".$list->id."\"><kbd class='btn rounded-circle mb-2 font-weight-bold'>X</kbd></a>";

                }
                echo '<input type="checkbox" class="checkbox ml-4" id="completeTask'.$task->id.'" name="completeTask'.$task->id.'"';
                if($task->completed){
                    echo ' checked>';
                    echo "<style type='text/css'> .task".$task->id." { text-decoration: line-through;} </style>";
                }
                else{
                    echo '>';
                }
                echo '<kbd class="ml-1" for="completeTask'.$task->id.'">Compléter</kbd>';
                echo "</div>";
            }
            echo "</div>";
            echo "</div>";
            echo "<div class='text-center'>";
            echo "<button type=\"submit\" class=\"text-center h5 btn-primary p-1 rounded btn\">Valider la complétion</button>";
            echo "</div>";
        if(!$isPrivate){
            echo "<input type=\"hidden\" name=\"action\" value=\"valPublicTask\">";
        }else{
            echo "<input type=\"hidden\" name=\"action\" value=\"valPrivateTask\">";
        }
        if(isset($page)) {echo "<input type=\"hidden\" name=\"p\" value=\"$page\">";}
        echo "<input type=\"hidden\" name=\"parent\" value=\"$list->id\">";
        echo "</form>";

    }else{
        echo "<h3 align='center'>Pas de tâche pour cette liste</h3>";
    }
    ?>
    <div align="center" class="container">
        <nav class="pagination justify-content-center w-100">
            <?php
            if(isset($tasks) && count($tasks) > 0) {
                if(isset($page) && isset($nbPage) && isset($isPrivate) && isset($list)) {
                    if ($page != 1) {
                        echo "<li class='page-item'><a href=\"index.php?action=".(($isPrivate) ? "viewListPrivate" : "viewList")."&id=".$list->id."&p=1\" class='page-link'>1  </a></li>";
                        echo "<li class='page-item'><a href=\"index.php?action=".(($isPrivate) ? "viewListPrivate" : "viewList")."&id=".$list->id."&p=".($page - 1)."\"class='page-link'> < </a></li>";
                    }
                    echo "<span class='page-link'>$page</span>";
                    if ($page != $nbPage) {
                        echo "<li class='page-item'><a href=\"index.php?action=".(($isPrivate) ? "viewListPrivate" : "viewList")."&id=".$list->id."&p=".($page + 1)."\" class='page-link'> > </a></li>";
                        echo "<li class='page-item'><a href=\"index.php?action=".(($isPrivate) ? "viewListPrivate" : "viewList")."&id=".$list->id."&p=".($nbPage)."\" class='page-link'>".$nbPage."</a></li>";
                    }
                }
            }
            ?>
        </nav>
    </div>
    <div class="text-center mt-5">
        <a class="text-center h5 btn-secondary p-1 rounded" href="./index.php?action=viewAddTask<?= (isset($isPrivate) && $isPrivate) ? "Private" : "Public" ?><?= (isset($list) ? "&id=".$list->id : null)?>">Ajout d'une tache</a>
    </div>
    <script type="text/javascript" src="view/js/particle2.js"></script>
    </div>
</main>
</body>