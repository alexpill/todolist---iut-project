<?php
require_once(__DIR__.'/config/config.php');
require_once(__DIR__.'/config/Autoload.php');

require_once (__DIR__.'/twig/vendor/autoload.php');

$loader = new Twig_Loader_Filesystem(__DIR__.'/view/template');
$twig = new Twig_Environment($loader,['cache' => __DIR__.'/view/template/cache', 'debug' => true]);
$twig->addExtension(new Twig_Extension_Debug());

Autoload::load();

$control = new FrontControl();
?>