<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:29
 */

class TaskGateway
{
    private $con;

    /* @param Connection : instance of Connection to connect to
     * a database
     */
    public function  __construct(DBConnection $con)
    {
        $this->con = $con;
    }


    /* @return array : all the public tasks
     * @throws Exception
     */
    public function selectAllPublic() : array
    {
        $query = "SELECT * FROM public_tasks";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();
        $tasks = DBFactory_Tasks::create($results,'mysql');
        return (isset($tasks)) ? $tasks : array();

    }

    /* @param string : title of the task to insert
     * @param string : comment of the task to insert
     */
    public function insertPublic($title,$comment, $parent)
    {
      $query = "INSERT INTO public_tasks (`id`, `title`, `comment`,`parent_id`) VALUES (NULL, :title, :com, :parent)";
      $this->con->executeQuery($query, array(
          ':title' => array($title,PDO::PARAM_STR),
          ':com' => array($comment, PDO::PARAM_STR),
          ':parent' => array($parent, PDO::PARAM_INT)
      ));
    }

    /* @param string : ifd of the task to remove
     * @return bool : return if the task was remove or not
     */
    public function deletePublic($id) : bool
    {
        $query = "DELETE FROM public_tasks WHERE id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_STR)
        ));
    }


    /**
     * @param $page : page you want to see
     * @param $taskOnPage : number of tasks by pages
     * @return array : all the task from the $page page
     * @throws Exception
     */
    public function selectByPage($page, $parent, $taskOnPage) : array {
        $start = ($page - 1) * $taskOnPage;
        $query = "Select * from public_tasks where parent_id=:parent limit :start, :taskOnPage";
        $this->con->executeQuery($query, array(
            ':start' => array($start, PDO::PARAM_INT),
            ':taskOnPage' => array($taskOnPage, PDO::PARAM_INT),
            ':parent' => array($parent,PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $tasks = DBFactory_Tasks::create($results,'mysql');
        return (isset($tasks)) ? $tasks : array();
    }
    /**
     * @param $id
     * @param $pageP
     * @param int $taskOnPage
     * @return array
     * @throws Exception
     */
    public function selectByPagePrivate($id, $pageP, int $taskOnPage) : array
    {
        $start = ($pageP - 1) * $taskOnPage;
        $query = "SELECT * FROM private_tasks WHERE parent_id=:id limit :start, :taskOnPage";
        $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_INT),
            ':start' => array($start, PDO::PARAM_INT),
            ':taskOnPage' => array($taskOnPage, PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $tasks = DBFactory_Tasks::create($results,'mysql');
        return (isset($tasks)) ? $tasks : array();
    }

    /**
     * @param $title
     * @param $comment
     * @param $parent
     */
    public function insertPrivate($title, $comment, $parent)
    {
        $query = "INSERT INTO private_tasks (`id`, `title`, `comment`,`parent_id`) VALUES (NULL, :title, :com, :parent)";
        $this->con->executeQuery($query, array(
            ':title' => array($title,PDO::PARAM_STR),
            ':com' => array($comment, PDO::PARAM_STR),
            ':parent' => array($parent, PDO::PARAM_INT)
        ));
    }

    /**
     * @param $id
     * @return bool
     */
    public function deletePrivate($id)
    {
        $query = "DELETE FROM private_tasks WHERE id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_INT)
        ));
    }

    /**
     * @param $id
     * @param $parent
     * @param int $completed
     */
    public function changePublicTaskState($id, $parent, int $completed)
    {
        $query = "UPDATE public_tasks SET completed=:com WHERE (id=:id and parent_id=:parent)";
        $this->con->executeQuery($query, array(
            ':com' => array($completed,PDO::PARAM_INT),
            ':id' => array($id, PDO::PARAM_INT),
            ':parent' => array($parent, PDO::PARAM_INT)
        ));
    }

    /**
     * @param $id
     * @param int $completed
     * @param $parentId
     */
    public function changePrivateTaskState($id, int $completed, $parentId)
    {
        $query = "UPDATE private_tasks SET completed=:com WHERE (id=:id AND parent_id=:parentId)";
        $this->con->executeQuery($query, array(
            ':com' => array($completed,PDO::PARAM_INT),
            ':id' => array($id, PDO::PARAM_STR),
            ':parentId' => array($parentId,PDO::PARAM_INT)
        ));
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function tasksFromListPrivate($id)
    {
        $query = "SELECT * FROM private_tasks WHERE parent_id=:id";
        $this->con->executeQuery($query,array(
            ':id' => array($id,PDO::PARAM_STR)
        ));
        $results = $this->con->getResults();
        $tasks = DBFactory_Tasks::create($results,'mysql');
        return (isset($tasks)) ? $tasks : array();
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function tasksFromList($id)
    {
        $query = "SELECT * FROM public_tasks WHERE parent_id=:id";
        $this->con->executeQuery($query,array(
            ':id' => array($id,PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $tasks = DBFactory_Tasks::create($results,'mysql');
        return (isset($tasks)) ? $tasks : array();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteAllPublicFromParent($id)
    {
        $query = "DELETE FROM public_tasks WHERE parent_id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_INT)
        ));
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteAllPrivateFromParent($id)
    {
        $query = "DELETE FROM private_tasks WHERE parent_id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_INT)
        ));
    }


}
