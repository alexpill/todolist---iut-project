<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 26/12/2018
 * Time: 14:30
 */

class UserGateway
{
    private $con;

    /**
     * UserGateway constructor.
     * @param DBConnection $connection
     */
    public function __construct(DBConnection $connection)
    {
        $this->con = $connection;
    }

    /**
     * @param $login
     * @return null
     */
    public function checkUser($login){
        $query = "SELECT pass FROM User WHERE login = :log";
        $this->con->executeQuery($query,array(
            ':log' => array($login,PDO::PARAM_STR)
        ));
        $result = $this->con->getResults();
        if(isset($result))
        {
            if(isset($result[0])){
                return $result[0]['pass'];
            }else{
                return null;
            }
        }
        else{
            return null;
        }
    }

    /**
     * @param $login
     * @param $mdp
     */
    public function addUser($login, $mdp){
        $query = "INSERT INTO User VALUES(NULL,:log,:pass)";
        $this->con->executeQuery($query,array(
            ':log' => array($login,PDO::PARAM_STR),
            ':pass' => array(password_hash($mdp,PASSWORD_DEFAULT),PDO::PARAM_STR)
        ));
    }

    /**
     * @return array
     * @throws Exception
     */
    public function allUser(){
        $query = "Select * from User";
        $this->con->executeQuery($query);
        $result = $this->con->getResults();
        $users = DBFactory_User::create($result,'mysql');
        return $users;
    }

    /**
     * @param $id
     * @return null
     * @throws Exception
     */
    public function selectById($id)
    {
        $query = "Select * from User where id=:id";
        $this->con->executeQuery($query,array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
        $result = $this->con->getResults();
        $users = DBFactory_User::create($result,'mysql');
        return (isset($users)) ? $users[0] : null;

    }

    /**
     * @param $id
     * @return bool
     */
    public function delUSer($id)
    {
        $query = "delete from User where id=:id";
        return $this->con->executeQuery($query,array(
            ':id' => array($id, PDO::PARAM_INT)
        ));
    }

}