<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 20/12/2018
 * Time: 16:19
 */

class AdminGateway
{

    private $con;

    /**
     * AdminGateway constructor.
     * @param DBConnection $connection
     */
    public function __construct(DBConnection $connection)
    {
        $this->con = $connection;
    }

    /**
     * @param $login
     * @return null
     */
    public function checkAdmin($login){
        $query = "SELECT pass FROM Admin WHERE login = :log";
        $this->con->executeQuery($query,array(
            ':log' => array($login,PDO::PARAM_STR)
        ));
        $result = $this->con->getResults();
        if(isset($result))
        {
            return $result[0]['pass'];
        }
        else{
            return null;
        }
    }

    /**
     * @param $login
     * @param $mdp
     */
    public function addAdmin($login, $mdp){
        $query = "INSERT INTO Admin VALUES(NULL,:log,:pass)";
        $this->con->executeQuery($query,array(
           ':log' => array($login,PDO::PARAM_STR),
           ':pass' => array(password_hash($mdp,PASSWORD_DEFAULT),PDO::PARAM_STR)
        ));

    }

}