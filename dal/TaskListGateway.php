<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:29
 */

class TaskListGateway
{
    private $con;

    /* @param Connection : instance of Connection to connect to
     * a database
     */
    public function  __construct(DBConnection $con)
    {
        $this->con = $con;
    }


    /* @return array : all the public taskLists
     * @throws Exception
     */
    public function selectAllPublic() : array
    {
        $query = "SELECT * FROM public_lists";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql');
        return (isset($taskLists)) ? $taskLists : array();

    }

    /* @param string : title of the taskList to insert
     * @param string : comment of the taskList to insert
     */
    public function insertPublic($title,$comment)
    {
        $query = "INSERT INTO public_lists (`id`, `title`, `comment`) VALUES (NULL, :title, :com)";
        $this->con->executeQuery($query, array(
            ':title' => array($title,PDO::PARAM_STR),
            ':com' => array($comment, PDO::PARAM_STR)
        ));
    }

    /* @param string : ifd of the taskList to remove
     * @return bool : return if the taskList was remove or not
     */
    public function deletePublic($id) : bool
    {
        $query = "DELETE FROM public_lists WHERE id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_STR)
        ));
    }


    /**
     * @param $page : page you want to see
     * @param $taskListOnPage : number of taskLists by pages
     * @return array : all the taskList from the $page page
     * @throws Exception
     */
    public function selectByPage($page, $taskListOnPage) : array {
        $start = ($page - 1) * $taskListOnPage;
        $query = "Select * from public_lists limit :start, :taskListOnPage";
        $this->con->executeQuery($query, array(
            ':start' => array($start, PDO::PARAM_INT),
            ':taskListOnPage' => array($taskListOnPage, PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql');
        return (isset($taskLists)) ? $taskLists : array();
    }

    /**
     * @param string $user
     * @return array
     * @throws Exception
     */
    public function selectAllPrivate(string $user) : array
    {
        $query = "SELECT * FROM private_lists WHERE owner=:user";
        $this->con->executeQuery($query,array(
            ':user' => array($user,PDO::PARAM_STR)
        ));
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql',true);
        return (isset($taskLists)) ? $taskLists : array();
    }

    /**
     * @param string $user
     * @param $pageP
     * @param int $taskListOnPage
     * @return array
     * @throws Exception
     */
    public function selectByPagePrivate(string $user, $pageP, int $taskListOnPage) : array
    {
        $start = ($pageP - 1) * $taskListOnPage;
        $query = "SELECT * FROM private_lists WHERE owner=:user limit :start, :taskListOnPage";
        $this->con->executeQuery($query, array(
            ':user' => array($user,PDO::PARAM_STR),
            ':start' => array($start, PDO::PARAM_INT),
            ':taskListOnPage' => array($taskListOnPage, PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql',true);
        return (isset($taskLists)) ? $taskLists : array();
    }

    /**
     * @param $log
     * @param $title
     * @param $comment
     */
    public function insertPrivate($log, $title, $comment)
    {
        $query = "INSERT INTO private_lists (`id`, `title`, `comment`,`owner`) VALUES (NULL, :title, :com,:log)";
        $this->con->executeQuery($query, array(
            ':title' => array($title,PDO::PARAM_STR),
            ':com' => array($comment, PDO::PARAM_STR),
            ':log' => array($log,PDO::PARAM_STR)
        ));
    }

    /**
     * @param $id
     * @return bool
     */
    public function deletePrivate($id)
    {
        $query = "DELETE FROM private_lists WHERE id=:id";
        return $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_STR)
        ));
    }

    /**
     * @param $id
     * @param int $completed
     */
    public function changePublicTaskListState($id, int $completed)
    {
        $query = "UPDATE public_lists SET completed=:com WHERE id=:id";
        $this->con->executeQuery($query, array(
            ':com' => array($completed,PDO::PARAM_INT),
            ':id' => array($id, PDO::PARAM_STR)
        ));
    }

    /**
     * @param $id
     * @param int $completed
     * @param string $login
     */
    public function changePrivateTaskListState($id, int $completed, string $login)
    {
        $query = "UPDATE private_lists SET completed=:com WHERE (id=:id AND owner=:user)";
        $this->con->executeQuery($query, array(
            ':com' => array($completed,PDO::PARAM_INT),
            ':id' => array($id, PDO::PARAM_STR),
            ':user' => array($login,PDO::PARAM_STR)
        ));
    }

    /**
     * @param $id
     * @return null
     * @throws Exception
     */
    public function selectByIdPublic($id)
    {
        $query = "SELECT * FROM public_lists WHERE id=:id";
        $this->con->executeQuery($query, array(
           ':id' => array($id,PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql');
        return (isset($taskLists)) ? $taskLists[0] : null;
    }

    /**
     * @param $id
     * @return null
     * @throws Exception
     */
    public function selectByIdPrivate($id)
    {
        $query = "SELECT * FROM private_lists WHERE id=:id";
        $this->con->executeQuery($query, array(
            ':id' => array($id,PDO::PARAM_INT)
        ));
        $results = $this->con->getResults();
        $taskLists = DBFactory_TasksLists::create($results,'mysql',true);
        return (isset($taskLists)) ? $taskLists[0] : null;
    }

    /**
     * @param $login
     * @return bool
     */
    public function deleteAllPrivateFromUser($login)
    {
        $query = "DELETE FROM private_lists WHERE owner=:login";
        return $this->con->executeQuery($query, array(
            ':login' => array($login,PDO::PARAM_STR)
        ));
    }

}
