<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:15
 */

class DBConnection extends PDO
{
    private $stmt;

    /* @param $dsn : Data source name
     * @param $username : to connect to database
     * @param $passwd : to connect to database
     */
    public function  __construct($dsn, $username, $passwd)
    {
        parent::__construct($dsn, $username, $passwd);
        $this->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    }

    /* @param string $query
     * @param array $parameters
     * @return bool Returns 'true' on success, 'false' otherwise
     */
    public function executeQuery($query, array $parameters = [])
    {
        $this->stmt = parent::prepare($query);
        foreach ($parameters as $name => $value){
            $this->stmt->bindValue($name,$value[0],$value[1]); }
        return $this->stmt->execute();
    }

    /* @return array Return an array of the query results
     *
     */
    public function getResults()
    {
        return $this->stmt->fetchall();
    }

}
