-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 14 jan. 2019 à 10:45
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `todolist`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `pass` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `login`, `pass`) VALUES
(2, 'admin', '$2y$10$kT89uAZJkpAF3GmCr526iOiCT3FV.jz0JyiEW4R71fuVe0a1bVOQm');

-- --------------------------------------------------------

--
-- Structure de la table `private_lists`
--

DROP TABLE IF EXISTS `private_lists`;
CREATE TABLE IF NOT EXISTS `private_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(144) NOT NULL,
  `comment` text NOT NULL,
  `owner` varchar(50) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `private_lists`
--

INSERT INTO `private_lists` (`id`, `title`, `comment`, `owner`, `completed`) VALUES
(17, 'lol', 'lol', 'user ', 0),
(18, 'haha', 'hoho', 'aurel', 0),
(16, 'Chouchou', 'Beignet', 'user ', 1);

-- --------------------------------------------------------

--
-- Structure de la table `private_tasks`
--

DROP TABLE IF EXISTS `private_tasks`;
CREATE TABLE IF NOT EXISTS `private_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(144) NOT NULL,
  `comment` text NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `private_tasks`
--

INSERT INTO `private_tasks` (`id`, `title`, `comment`, `completed`, `parent_id`) VALUES
(23, 'hehe', 'hihi', 0, 18),
(22, 'Une tres belle tache', 'une tres tres belle tache', 0, 16),
(21, 'Une belle tache', 'Une belle tache', 1, 16);

-- --------------------------------------------------------

--
-- Structure de la table `public_lists`
--

DROP TABLE IF EXISTS `public_lists`;
CREATE TABLE IF NOT EXISTS `public_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(144) NOT NULL,
  `comment` text NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `public_lists`
--

INSERT INTO `public_lists` (`id`, `title`, `comment`, `completed`) VALUES
(88, 'List 8', 'Je suis la liste 8', 0),
(87, 'List 7', 'Je suis la liste 7', 1),
(86, 'List 6 je suis la liste 6', '', 1),
(85, 'List 5', 'Je suis la liste 5', 0),
(84, 'Liste 4 je suis la liste 4', '', 0),
(83, 'List 3', 'Je suis la list 3', 0),
(81, 'List 1', 'Je suis la liste 1', 0),
(82, 'List 2 ', 'Je suis la list 2', 0),
(89, 'List 9', 'Je suis la list 9', 0);

-- --------------------------------------------------------

--
-- Structure de la table `public_tasks`
--

DROP TABLE IF EXISTS `public_tasks`;
CREATE TABLE IF NOT EXISTS `public_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(144) NOT NULL,
  `comment` text NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `public_tasks`
--

INSERT INTO `public_tasks` (`id`, `title`, `comment`, `completed`, `parent_id`) VALUES
(27, 'Tache 6', 'Tache 6', 0, 88),
(26, 'tache 5 ', 'tache 5', 1, 88),
(25, 'tache 4', 'tache 4', 0, 88),
(24, 'tache 3', 'Tache 3', 1, 88),
(23, 'tach2', 'tache 2', 0, 88),
(22, 'Tache 1', 'Tache 1', 0, 88),
(28, 'Tache 7', 'tache 7', 0, 88);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `pass` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `pass`) VALUES
(1, 'user', '$2y$10$kT89uAZJkpAF3GmCr526iOiCT3FV.jz0JyiEW4R71fuVe0a1bVOQm'),
(9, 'aurel', '$2y$10$gtfXzSZAT4mXkc0F5yuPY.kl82KXVYwIkKT04wFu/FV8sit3lVI72');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
