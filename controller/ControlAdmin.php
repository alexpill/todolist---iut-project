<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:28
 */

class ControlAdmin
{
    /**
     * ControlAdmin constructor.
     */
    public function __construct()
    {
        global $rep, $views;
        $viewError = array();
        try{
            $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : NULL; //here to prevent warning in debug mode
            //TODO : remove up

            switch ($action)
            {
                case NULL:
                    break;

                case "deconnexionAdmin":
                    $this->deco();
                    break;

                case "delUser":
                    $this->delUser();
                    break;


            }
        }catch(PDOException $e){
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
        catch(Exception $e)
        {
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
    }

    /**
     *
     */
    private function deco()
    {
        $m = new ModelAdmin();
        $m->deconnexion();
        $_REQUEST['action'] = NULL;
        header('Location: index.php');
    }

    /**
     * @throws Exception
     */
    private function delUser()
    {
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelAdmin();
        $m->delUser($id);
        $_REQUEST['action'] = NULL;
        header('Location: index.php');

    }

}