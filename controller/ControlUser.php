<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:28
 */

class ControlUser
{
    private $user;

    /**
     * ControlUser constructor.
     */
    public function __construct()
    {
        global $rep, $views;
        $this->user = ModelUser::isUser();
        $viewError = array();
        try{
            $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : NULL; //here to prevent warning in debug mode
            //TODO : remove up

            switch ($action)
            {
                case NULL:
                    break;

                case "deconnexionUser":
                    $this->deco();
                    break;

                case "viewAddPrivate":
                    $this->viewAddTaskList();
                    break;

                case "viewListPrivate":
                    $this->viewTaskListPrivate();
                    break;

                case "addPrivate":
                    $this->addTaskListPrivate();
                    break;

                case "addTaskPrivate":
                    $this->addTaskPrivate();
                    break;

                case "delPrivate":
                    $this->removeTaskListPrivate();
                    break;

                case "valPrivate":
                    $this->changeCompleted();
                    break;

                case "valPrivateTask":
                    $this->changeCompletedTask();
                    break;

                case "delTaskPrivate":
                    $this->removeTaskPrivate();
                    break;

                case "viewAddTaskPrivate":
                    $this->viewAddTask();
                    break;


                default:
                    $viewError[] = "Erreur innatendu";
                    require($rep.$views['error']);
                    break;

            }
        }catch(PDOException $e){
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
        catch(Exception $e)
        {
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
    }

    /**
     *
     */
    private function deco()
    {
        global $rep, $view;
        $m = new ModelUser();
        $m->deconnexion();
        $_REQUEST['action'] = NULL;
        header('Location: index.php#privateTaskListDisplay');
    }

    /**
     *
     */
    private function addTaskListPrivate()
    {
        global $rep, $views;
        $title = $_POST['title'];
        $comment = $_POST['comment'];
        /*if(!Validation::validateString($title)||!Validation::validateString($comment)){
            throw new Exception("Valeur invalide lors de l'ajout");
        }*/
        $title = Validation::sanitizeString($title);
        $comment = Validation::sanitizeString($comment);
        if($title == "" || $comment == ""){
            $err[] = "Field cannot be empty";
            $isPrivate = true;
            require $rep.$views['add_task_list'];
        }
        else{
            $m = new ModelUser();
            $m->addPrivateTaskList($this->user->getLogin(),$title,$comment);
            $_REQUEST['action'] = NULL;
            header('Location: index.php#privateTaskListDisplay');
        }
    }

    /**
     * @throws Exception
     */
    private function removeTaskListPrivate(){
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelUser();
        if(!$m->removeTaskListPrivate($id)) {
            throw new Exception("Id invalide");
        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php#privateTaskListDisplay');
    }

    /**
     *
     */
    private function changeCompleted()
    {
        $pageP = isset($_REQUEST['pp']) ? abs(intval($_REQUEST['pp'])) : 1 ;
        $pageP = $pageP == 0 ? 1 : $pageP;
        $m = new ModelUser();
        $TaskLists = $m->TaskList_by_page($pageP,$this->user->getLogin());
        foreach ($TaskLists as $t)
        {
            $completed = isset($_POST['completeTaskListPri'.$t->id]) ? 1 : 0;
            if($t->completed != $completed){
                $m->changePrivateState($t->id,$completed,$this->user->getLogin());
            }

        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php#privateTaskListDisplay');
    }

    /**
     * @throws Exception
     */
    private function viewTaskListPrivate()
    {
        global $rep, $views, $taskByPage;
        $c = new ModelCookie();
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelUser();
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $m = new ModelUser();
        $totTaskList = count($m->all_task_private($id));
        $nbPage = ceil($totTaskList / $c->getNbTaskByPage());
        $tasks = $m->task_by_page($page,$id);
        $list = $m->taskListFromId($id);
        $isPrivate = $list->isPrivate;
        $_REQUEST['action'] = NULL;
        require($rep.$views['list_view']);

    }

    /**
     *
     */
    private function changeCompletedTask()
    {
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $parent = isset($_REQUEST['parent']) ? abs(intval($_REQUEST['parent'])) : 1 ;
        $m = new ModelUser();
        $tasks = $m->task_by_page($page,$parent);
        foreach ($tasks as $t)
        {
            $completed = isset($_POST['completeTask'.$t->id]) ? 1 : 0;
            if($t->completed != $completed){
                $m->changePrivateStateTask($t->id,$completed,$parent);
            }

        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php?action=viewListPrivate&id='.$parent);
    }

    /**
     * @throws Exception
     */
    private function removeTaskPrivate()
    {
        $id = $_GET['idT'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelUser();
        $idL = $_GET['id'];
        if(!Validation::validateInt($idL)){
            throw new Exception("Index incorrecte");
        }
        $list = $m->taskListFromId($idL);
        if(!$m->removeTaskPrivate($id)) {
            throw new Exception("Id invalide");
        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php?action=viewListPrivate&id='.$list->id);
    }

    /**
     * @throws Exception
     */
    private function viewAddTask()
    {
        global $rep, $views;
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelUser();
        $parent = $m->taskListFromId($id);
        $isPrivate = $parent->isPrivate;
        require($rep.$views['add_task']);
    }

    /**
     * @throws Exception
     */
    private function addTaskPrivate()
    {
        $title = $_POST['title'];
        $comment = $_POST['comment'];
        $parent = $_POST['parent'];
        if(!Validation::validateInt($parent)){
            throw new Exception("Index incorrecte");
        }
        /*if(!Validation::validateString($title)||!Validation::validateString($comment)){
            throw new Exception("Valeur invalide lors de l'ajout");
        }*/
        $title = Validation::sanitizeString($title);
        $comment = Validation::sanitizeString($comment);
        $m = new ModelUser();
        if($title == "" || $comment == ""){
//            $err[] = "Field cannot be empty";
            $_GET['id'] = $parent;
            $parentT = $m->taskListFromId($parent);
            $isPrivate = $parentT->isPrivate;
            $this->viewAddTask();
        }else {
            $m->add_private_task($title, $comment, $parent);
            $_REQUEST['action'] = NULL;
            header('Location: index.php?action=viewListPrivate&id=' . $parent);
        }
    }

    /**
     *
     */
    private function viewAddTaskList()
    {
        global $rep, $views;
        $isPrivate = true;
        require($rep.$views['add_task_list']);
    }

}