<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:28
 */

class ControlVisitor
{
    /**
     * ControlVisitor constructor.
     */
    public function __construct()
    {
        global $rep, $views;
        $viewError = array();
        try{
            $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : NULL; //here to prevent warning in debug mode
            //TODO : remove up

            switch ($action)
            {
                case NULL:
                    $this->ReInit();
                    break;

                case "addPublic":
                    $this->addTaskListPublic();
                    break;

                case "addTaskPublic":
                    $this->addTaskPublic();
                    break;

                case "delPublic":
                    $this->removeTaskListPublic();
                    break;

                case "delTaskPublic":
                    $this->removeTaskPublic();
                    break;

                case "viewAdd":
                    $this->viewAddTaskList();
                    break;

                case "viewConnectionAdmin":
                    $typeConnection = "Admin";
                    require($rep.$views['connection']);
                    break;

                case "viewConnectionUser":
                    $typeConnection = "User";
                    require($rep.$views['connection']);
                    break;

                case "viewInscriptionUser":
                    require($rep.$views['inscription']);
                    break;
                    break;

                case "connectionAdmin":
                    $this->connectionAdmin();
                    break;

                case "connectionUser":
                    $this->connectionUser();
                    break;

                case "inscriptionUser":
                    $this->inscriptionUser();
                    break;

                case "valPublic":
                    $this->changeCompleted();
                    break;

                case "valPublicTask":
                    $this->changeCompletedTask();
                    break;

                case "viewList":
                    $this->viewTaskList();
                    break;

                case "viewAddTaskPublic":
                    $this->viewAddTask();
                    break;

                case "changeNbByPage":
                    $this->changeCookieNbPage();
                    break;


                default:
                    $viewError[] = "Erreur innatendu Visitor";
                    require($rep.$views['error']);
                    break;
            }
        }catch(PDOException $e){
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
        catch(Exception $e)
        {
            $viewError[] = $e->getMessage();
            require($rep.$views['error']);
        }
    }

    /**
     * Function that init the firstView
     * @throws Exception
     */
    private function ReInit()
    {
        global $rep, $views, $taskListByPage, $twig;
        $a = ModelAdmin::isAdmin();
        $u = ModelUser::isUser();
        $c = new ModelCookie();
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $m = new ModelVisitor();
        $totTaskList = count($m->all_taskList_public());
        $nbPage = ceil($totTaskList / $c->getNbListByPage());
        $taskLists = $m->taskList_by_page($page);
        if(isset($u)){
            $pageP = isset($_REQUEST['pp']) ? abs(intval($_REQUEST['pp'])) : 1 ;
            $pageP = $pageP == 0 ? 1 : $pageP;
            $mP = new ModelUser();
            $totTaskListP = count($mP->all_taskList_private($u->getLogin()));
            $nbPageP = ceil($totTaskListP / $c->getNbListByPage());
            $taskListsP = $mP->taskList_by_page($pageP,$u->getLogin());
        }
        if(isset($a)){
            $mA = new ModelAdmin();
            $users = $mA->selectAllUser();
        }
//        require ($rep.$views['first_view']);
        if(!isset($a) & !isset($u)){
            echo $twig->render('firstTemplate.php', ['taskLists' => $taskLists, 'totTaskList' => $totTaskList,
                'page' => $page, 'nbPage' => $nbPage]);
        }
        if(isset($u)){
            echo $twig->render('firstTemplate.php', ['taskLists' => $taskLists, 'totTaskList' => $totTaskList,
                'page' => $page, 'nbPage' => $nbPage, 'u' => $u, 'pageP' => $pageP, 'totTaskListP' => $totTaskListP,
                'nbPageP' => $nbPageP, 'taskListsP' => $taskListsP]);
        }
        if(isset($a)){
            echo $twig->render('firstTemplate.php', ['a' => $a, 'users' => $users]);
        }

    }

    /**
     * method to add a public taskList
     * @throws Exception
     */
    private function addTaskListPublic()
    {
        global $rep, $views;
        $title = $_POST['title'];
        $comment = $_POST['comment'];
        /*if(!Validation::validateString($title)||!Validation::validateString($comment)){
            throw new Exception("Valeur invalide lors de l'ajout");
        }*/
        $title = Validation::sanitizeString($title);
        $comment = Validation::sanitizeString($comment);
        $_REQUEST['action'] = NULL;
        if($title == "" || $comment == ""){
            $err[] = "Field cannot be empty";
            require $rep.$views['add_task_list'];
        }
        else{
            $m = new ModelVisitor();
            $m->add_public_taskList($title,$comment);
            $this->ReInit();
        }

    }

    /**
     * Method to remove a public taskList
     * @throws Exception : whenever there is a problem on index
     */
    private function removeTaskListPublic(){
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelVisitor();
        if(!$m->removeTaskListPublic($id)) {
            throw new Exception("Id invalide");
        }
        $_REQUEST['action'] = NULL;
        $this->ReInit();
    }

    /**
     * @throws Exception
     */
    private function connectionAdmin()
    {
        global $rep,$views;
        //pas obliger car deja dans model connexion
        $login = Validation::sanitizeString($_REQUEST['login']);
        $mdp = Validation::sanitizeString($_REQUEST['mdp']);
        //TODO : sanitize password
//        $mdp = (Validation::validatePassword($_REQUEST['mdp'])) ? $_REQUEST['mdp'] : "";
        $m = new ModelAdmin();
        $a = $m->connection($login,$mdp);
        if(!isset($a)){
            $err[] = "Login or Password Invalid";
            $typeConnection = "Admin";
            require $rep.$views['connection'];
        }else{
            $this->ReInit();
        }

    }

    /**
     * @throws Exception
     */
    private function connectionUser()
    {
        global $rep,$views;
        //pas obliger car deja dans model connexion
        $login = Validation::sanitizeString($_REQUEST['login']);
        $mdp = Validation::sanitizeString($_REQUEST['mdp']);
        //TODO : sanitize password
//        $mdp = (Validation::validatePassword($_REQUEST['mdp'])) ? $_REQUEST['mdp'] : "";
        $m = new ModelUser();
        $u = $m->connection($login,$mdp);
        if(!isset($u)){
            $err[] = "Login or Password Invalid";
            $typeConnection = "User";
            require $rep.$views['connection'];
        }
        else{
            $this->ReInit();
        }

    }

    /**
     * @throws Exception
     */
    private function inscriptionUser()
    {
        global $rep,$views;
        $login = Validation::sanitizeString($_REQUEST['login']);
        $mdp = Validation::sanitizeString($_REQUEST['mdp']);
        $mdpVerif = Validation::sanitizeString($_REQUEST['mdp_verify']);
        $m = new ModelUser();
        $err = $m->inscription($login,$mdp,$mdpVerif);
        if(isset($err)){
            require($rep.$views['inscription']);
        }
        else{
            $this->ReInit();
        }
    }

    /**
     * @throws Exception
     */
    private function changeCompleted()
    {
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $m = new ModelVisitor();
        $taskLists = $m->taskList_by_page($page);
        foreach ($taskLists as $t)
        {
            $completed = isset($_POST['completeTaskList'.$t->id]) ? 1 : 0;
            if($t->completed != $completed){
                $m->changePublicState($t->id,$completed);
            }

        }
        $_REQUEST['action'] = NULL;
        $this->ReInit();

    }

    /**
     * @throws Exception
     */
    private function viewTaskList()
    {
        global $rep, $views, $taskByPage;
        $c = new ModelCookie();
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelVisitor();
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $m = new ModelVisitor();
        $totTaskList = count($m->all_task_public($id));
        $nbPage = ceil($totTaskList / $c->getNbTaskByPage());
        $tasks = $m->task_by_page($page,$id);
/*        $tasks = $m->tasksFromList($id);*/
        $list = $m->taskListFromId($id);
        $isPrivate = $list->isPrivate;
        $_REQUEST['action'] = NULL;
        require($rep.$views['list_view']);
    }

    /**
     *
     */
    private function changeCompletedTask()
    {
        $page = isset($_REQUEST['p']) ? abs(intval($_REQUEST['p'])) : 1 ;
        $page = $page == 0 ? 1 : $page;
        $parent = isset($_REQUEST['parent']) ? abs(intval($_REQUEST['parent'])) : 1 ;
        $m = new ModelVisitor();
        $tasks = $m->task_by_page($page,$parent);
        foreach ($tasks as $t)
        {
            $completed = isset($_POST['completeTask'.$t->id]) ? 1 : 0;
            if($t->completed != $completed){
                $m->changePublicStateTask($t->id,$completed,$parent);
            }

        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php?action=viewList&id='.$parent);

    }

    /**
     * @throws Exception
     */
    private function removeTaskPublic()
    {
        $id = $_GET['idT'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelVisitor();
        $idL = $_GET['id'];
        if(!Validation::validateInt($idL)){
            throw new Exception("Index incorrecte");
        }
        $list = $m->taskListFromId($idL);
        if(!$m->removeTaskPublic($id)) {
            throw new Exception("Id invalide");
        }
        $_REQUEST['action'] = NULL;
        header('Location: index.php?action=viewList&id='.$list->id);
    }

    /**
     * @throws Exception
     */
    private function addTaskPublic()
    {
        global $rep, $views;
        $title = $_POST['title'];
        $comment = $_POST['comment'];
        $parent = $_POST['parent'];
        if(!Validation::validateInt($parent)){
            throw new Exception("Index incorrecte");
        }
        /*if(!Validation::validateString($title)||!Validation::validateString($comment)){
            throw new Exception("Valeur invalide lors de l'ajout");
        }*/
        $title = Validation::sanitizeString($title);
        $comment = Validation::sanitizeString($comment);
        if($title == "" || $comment == ""){
//            $err[] = "Field cannot be empty";
            $_GET['id'] = $parent;
            $this->viewAddTask();
        }else {

            $m = new ModelVisitor();
            $m->add_public_task($title, $comment, $parent);
            $_REQUEST['action'] = NULL;
            header('Location: index.php?action=viewList&id=' . $parent);
        }
    }

    /**
     * @throws Exception
     */
    private function viewAddTask()
    {
        global $rep, $views;
        $id = $_GET['id'];
        if(!Validation::validateInt($id)){
            throw new Exception("Index incorrecte");
        }
        $m = new ModelVisitor();
        $parent = $m->taskListFromId($id);
        $isPrivate = $parent->isPrivate;
        require($rep.$views['add_task']);
    }

    /**
     *
     */
    private function viewAddTaskList()
    {
        global $rep, $views;
        $isPrivate = false;
        require($rep.$views['add_task_list']);
    }

    /**
     * @throws Exception
     */
    private function changeCookieNbPage()
    {
        $nbListByPage = $_POST['nbListByPage'];
        $nbTaskByPage = $_POST['nbTaskByPage'];
        if(!Validation::validateInt($nbListByPage) || !Validation::validateInt($nbTaskByPage)){
            throw new Exception("Valeurs incorrectes");
        }
        $c = new ModelCookie();
        $c->setNbListByPage($nbListByPage);
        $c->setNbTaskByPage($nbTaskByPage);
        Header('Location: '.$_SERVER['PHP_SELF']);
        $this->ReInit();
    }

}
