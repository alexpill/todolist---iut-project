<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 20/12/2018
 * Time: 16:41
 */

class FrontControl
{
    /**
     * FrontControl constructor.
     */
    public function __construct()
    {
        session_start();
        global $rep, $views;
        $listeAction_Admin = array('deconnexionAdmin','delUser');
        $listeAction_User = array('deconnexionUser','delPrivate','valPrivate','addPrivate',
            'viewAddPrivate','viewListPrivate','valPrivateTask','delTaskPrivate','viewAddTaskPrivate',
            'addTaskPrivate');
        try{
            $a=ModelAdmin::isAdmin();
            $u=ModelUser::isUser();
            $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : NULL; //here to prevent warning in debug mode
            //TODO : remove up
            if(in_array($action,$listeAction_Admin)){
                if($a == null){
                    require_once($rep.$views['connection_admin']);
                }
                else{
                    new ControlAdmin();
                }
            }
            else if(in_array($action,$listeAction_User)){
                if($u == null){
                    require_once($rep.$views['connection_user']);
                }
                else{
                    new ControlUser();
                }
            }
            else{
                new ControlVisitor();
            }
        }
        catch (Exception $e){
            $viewError[] = $e->getMessage();
            require $rep.$views['error'];
        }
    }

}