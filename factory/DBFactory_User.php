<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 14/01/2019
 * Time: 00:03
 */

class DBFactory_User
{
    /**
     * @param $result
     * @param $param
     * @return array
     * @throws Exception
     */
    public static function create($result, $param){
        if(!isset($result)){
            throw new Exception('No data');
        }
        switch ($param){
            case 'mysql':
                return self::mysqlTasks($result);
                break;

            default:
                throw new Exception('Unknown database');
        }
    }

    /**
     * @param $result
     * @return array
     */
    private static function mysqlTasks($result){
        $tab = array();
        foreach ($result as $user){
            $tab[] = new User($user['login'],'user',$user['id']);
        }
        return $tab;
    }

}