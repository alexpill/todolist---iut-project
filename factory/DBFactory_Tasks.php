<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 13/01/2019
 * Time: 14:47
 */

class DBFactory_Tasks
{
    /**
     * @param $result
     * @param $param
     * @return array
     * @throws Exception
     */
    public static function create($result, $param){
        if(!isset($result)){
            throw new Exception('No data');
        }
        switch ($param){
            case 'mysql':
                return self::mysqlTasks($result);
                break;

            default:
                throw new Exception('Unknown database');
        }
    }

    /**
     * @param $result
     * @return array
     */
    private static function mysqlTasks($result){
        $tab = array();
        foreach ($result as $task){
            $tab[] = new Task($task['id'],$task['title'],$task['comment'],$task['completed']);
        }
        return $tab;
    }
}