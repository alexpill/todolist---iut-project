<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 13/01/2019
 * Time: 14:47
 */

class DBFactory_TasksLists
{
    /**
     * @param $result
     * @param $param
     * @param bool $private
     * @return array
     * @throws Exception
     */
    public static function create($result, $param, bool $private = false){
        if(!isset($result)){
            throw new Exception('No data');
        }
        switch ($param){
            case 'mysql':
                return self::mysqlTasks($result,$private);
                break;

            default:
                throw new Exception('Unknown database');
        }
    }

    /**
     * @param $result
     * @param bool $private
     * @return array
     */
    private static function mysqlTasks($result, bool $private){
        $tab = array();
        foreach ($result as $taskList){
            if ($private){
                $tab[] = new TaskList($taskList['id'],$taskList['title'],$taskList['comment'],$taskList['completed'],true);
            }
            else{
                $tab[] = new TaskList($taskList['id'],$taskList['title'],$taskList['comment'],$taskList['completed']);
            }
        }
        return $tab;
    }

}