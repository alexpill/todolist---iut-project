<?php

class Validation
{
    /**
     * @param $mail
     * @return bool
     */
    static function validateMail($mail) : bool {
    if(isset($mail)){
      return filter_var($mail,FILTER_VALIDATE_EMAIL);
    }
  }

    /**
     * @param $s
     * @return bool
     */
    static function validateString($s){
    if(isset($s)){
      return $s == filter_var($s,FILTER_SANITIZE_STRING);
    }
  }

    /**
     * @param string $s
     * @return string
     */
    static function sanitizeString(string $s) : string {
      if(isset($s)){
          return filter_var($s,FILTER_SANITIZE_STRING);
      }
  }

    /**
     * @param $url
     * @return mixed
     */
    static function validateURL($url){
    if(isset($url)){
      return filter_var($url,FILTER_SANITIZE_URL);
    }
  }

    /**
     * @param $int
     * @return mixed
     */
    static function validateInt($int){
    if(isset($int)){
      return filter_var($int,FILTER_VALIDATE_INT);
    }
  }

    /**
     * @param $float
     * @return mixed
     */
    static function validateFloat($float){
    if(isset($float)){
      return filter_var($float,FILTER_VALIDATE_FLOAT);
    }
  }

    /**
     * @param $pass
     * @return bool
     */
    static function validatePassword($pass) : bool {
    if(isset($pass)){
      return filter_var($pass,FILTER_VALIDATE_REGEXP, array(
        'options' => '^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$'));
    }
  }


}
