<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 11/01/2019
 * Time: 13:49
 */

class ModelCookie
{

    /**
     * @return int
     */
    public function getNbListByPage(){
        global $taskListByPage;
        if(!isset($_COOKIE['listByPage'])){$this->setNbListByPage($taskListByPage);}
        return (Validation::validateInt($_COOKIE['listByPage'])) ? $_COOKIE['listByPage'] : $taskListByPage;
    }

    /**
     * @param $value
     */
    public function setNbListByPage($value){
        global $cookieExpiration;
        $value = (Validation::validateInt($value)) ? $value : $this->getNbListByPage();
        setcookie('listByPage',$value,time()+$cookieExpiration);
    }

    /**
     * @return int
     */
    public function getNbTaskByPage(){
        global $taskByPage;
        if(!isset($_COOKIE['taskByPage'])){$this->setNbTaskByPage($taskByPage);}
        return (Validation::validateInt($_COOKIE['taskByPage'])) ? $_COOKIE['taskByPage'] : $taskByPage;
    }

    /**
     * @param $value
     */
    public function setNbTaskByPage($value){
        global $cookieExpiration;
        $value = (Validation::validateInt($value)) ? $value : $this->getNbTaskByPage();
        setcookie('taskByPage',$value,time()+$cookieExpiration);
    }



}