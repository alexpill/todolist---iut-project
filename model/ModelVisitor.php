<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 15:06
 */

class ModelVisitor
{
    private $taskListGateway;
    private $taskGateway;

    /**
     * ModelVisitor constructor.
     */
    public function __construct()
    {
        global $dsn, $login, $mdp;
        $dbCo = new DBConnection($dsn,$login,$mdp);
        $this->taskListGateway = new TaskListGateway($dbCo);
        $this->taskGateway = new TaskGateway($dbCo);
    }


    /**
     * @return array
     * @throws Exception
     */
    public function all_taskList_public()
    {
        return $this->taskListGateway->selectAllPublic();
    }

    /**
     * @param $title : title to add
     * @param $comment : comment to add
     */
    public function add_public_taskList($title, $comment)
    {
        $this->taskListGateway->insertPublic($title,$comment);
    }

    /**
     * @param $id : id to remove
     * @return bool : true of removed, false otherwise
     */
    public function removeTaskListPublic($id)
    {
        $this->taskGateway->deleteAllPublicFromParent($id);
        return $this->taskListGateway->deletePublic($id);
    }


    /**
     * @param $page
     * @return array
     * @throws Exception
     */
    public function taskList_by_page($page) : array{
        $c = new ModelCookie();
        return $this->taskListGateway->selectByPage($page,$c->getNbListByPage());
    }

    /**
     * @param $id
     * @param int $completed
     */
    public function changePublicState($id, int $completed)
    {
        $this->taskListGateway->changePublicTaskListState($id,$completed);
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function tasksFromList($id)
    {
        return $this->taskGateway->tasksFromList($id);
    }

    /**
     * @param $id
     * @return null
     * @throws Exception
     */
    public function taskListFromId($id)
    {
        return $this->taskListGateway->selectByIdPublic($id);
    }

    /**
     * @param $page
     * @param $parent
     * @return array
     * @throws Exception
     */
    public function task_by_page($page, $parent)
    {
        $c = new ModelCookie();
        return $this->taskGateway->selectByPage($page,$parent,$c->getNbTaskByPage());
    }

    /**
     * @param $id
     * @param int $completed
     * @param $parent
     */
    public function changePublicStateTask($id, int $completed, $parent)
    {
        $this->taskGateway->changePublicTaskState($id,$parent,$completed);
    }

    /**
     * @param $id
     * @return bool
     */
    public function removeTaskPublic($id)
    {
        return $this->taskGateway->deletePublic($id);
    }

    /**
     * @param string $title
     * @param string $comment
     * @param $parent
     */
    public function add_public_task(string $title, string $comment, $parent)
    {
        $this->taskGateway->insertPublic($title,$comment,$parent);
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function all_task_public($id)
    {
        return $this->taskGateway->tasksFromList($id);
    }
}
