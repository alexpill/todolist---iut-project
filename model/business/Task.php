<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:30
 */

class Task
{
    public $id;
    public $title;
    public $comment;
    public $completed;
    public $isPrivate;

    /**
     * Task constructor.
     * @param $id Id of the new task
     * @param $title title of the new task
     * @param $comment comment / content of the new task
     */
    public function __construct($id, $title, $comment, $c, $isPrivate = false)
    {
        $this->id = $id;
        $this->title = $title;
        $this->comment = $comment;
        $this->completed = $c;
        $this->isPrivate = $isPrivate;
    }

    /**
     * @return string
     */
    public function __toString()
    {
      return "id : $this->id , title : $this->title, comment : $this->comment, completed : $this->completed";
    }

}
