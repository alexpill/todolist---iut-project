<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 30/11/2018
 * Time: 14:30
 */

class TaskList
{
    public $id;
    public $title;
    public $comment;
    public $completed;
    public $isPrivate;

    /**
     * Task List constructor.
     * @param $id Id of the new Task List
     * @param $title title of the new Task List
     * @param $comment comment / content of the new Task List
     */
    public function __construct($id, $title, $comment, $c, $isPrivate = false)
    {
        $this->id = $id;
        $this->title = $title;
        $this->comment = $comment;
        $this->completed = $c;
        $this->isPrivate = $isPrivate;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "id : $this->id , title : $this->title, comment : $this->comment, completed : $this->completed";
    }

}
