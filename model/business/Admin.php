<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 20/12/2018
 * Time: 16:15
 */

class Admin
{
    private $login;
    private $role;

    /**
     * Admin constructor.
     * @param string $login
     * @param string $role
     */
    public function __construct(string $login, string $role) {
        $this->login = $login;
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }


}