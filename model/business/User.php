<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 26/12/2018
 * Time: 14:29
 */

class User
{
    private $login;
    private $role;
    private $id;

    /**
     * User constructor.
     * @param string $login
     * @param string $role
     * @param null $id
     */
    public function __construct(string $login, string $role, $id = null) {
        $this->login = $login;
        $this->role = $role;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}