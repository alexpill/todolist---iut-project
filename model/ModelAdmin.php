<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 20/12/2018
 * Time: 16:18
 */

class ModelAdmin
{
    private $adminGateway;
    private $userGateway;
    private $taskListGateway;
    private $taskGateway;

    /**
     * ModelAdmin constructor.
     */
    public function __construct()
    {
        global $dsn, $login, $mdp;
        $c = new DBConnection($dsn,$login,$mdp);
        $this->adminGateway = new AdminGateway($c);
        $this->userGateway = new UserGateway($c);
        $this->taskListGateway = new TaskListGateway($c);
        $this->taskGateway = new TaskGateway($c);
    }

    /**
     * @param $login
     * @param $mdp
     * @return Admin|null
     */
    public function connection($login, $mdp){
        $login = Validation::sanitizeString($login);
        $mdp = Validation::sanitizeString($_REQUEST['mdp']);
        //TODO : sanitize password
//        $mdp = (Validation::validatePassword($_REQUEST['mdp'])) ? $_REQUEST['mdp'] : "";
        $hash = $this->adminGateway->checkAdmin($login);
        if(password_verify($mdp,$hash)){
            $_SESSION['login'] = $login;
            $_SESSION['role'] = "admin"; //TODO : chiffrer ca
            return new Admin($login,"admin");
        }else{
            return null;
        }

    }

    /**
     * @return Admin|null
     */
    public static function isAdmin(){
        if(isset($_SESSION['login']) && isset($_SESSION['role']) && $_SESSION['role'] == "admin"){
            $login = Validation::sanitizeString($_SESSION['login']);
            $role = Validation::sanitizeString($_SESSION['role']);
            return new Admin($login, $role);
        }else{
            return null;
        }
    }

    /**
     *
     */
    public function deconnexion(){
        session_unset();
        session_destroy();
        $_SESSION = array();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function selectAllUser(){
        return $this->userGateway->allUser();
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function delUser($id)
    {
        $u = $this->userGateway->selectById($id);
        if (!empty($u->getLogin())) {
            $lists = $this->taskListGateway->selectAllPrivate($u->getLogin());
        }else{
            throw new Exception("USer don't exist");
        }
        foreach ($lists as $l){
            $this->taskGateway->deleteAllPrivateFromParent($l->id);
        }
        $this->taskListGateway->deleteAllPrivateFromUser($u->getLogin());
        $this->userGateway->delUSer($id);
    }

}