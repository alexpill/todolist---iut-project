<?php
/**
 * Created by PhpStorm.
 * User: alexp
 * Date: 26/12/2018
 * Time: 14:32
 */

class ModelUser
{
    private $userGateway;
    private $taskListGateway;
    private $taskGateway;

    /**
     * ModelUser constructor.
     */
    public function __construct()
    {
        global $dsn, $login, $mdp;
        $dbC = new DBConnection($dsn,$login,$mdp);
        $this->userGateway = new UserGateway($dbC);
        $this->taskListGateway = new TaskListGateway($dbC);
        $this->taskGateway = new TaskGateway($dbC);
    }

    /**
     * @param $login
     * @param $mdp
     * @return null|User
     */
    public function connection($login, $mdp){
        $login = Validation::sanitizeString($login);
        $mdp = Validation::sanitizeString($mdp);
        //TODO : sanitize password
//        $mdp = (Validation::validatePassword($_REQUEST['mdp'])) ? $_REQUEST['mdp'] : "";
        $hash = $this->userGateway->checkUser($login);
        if(password_verify($mdp,$hash)){
            $_SESSION['login'] = $login;
            $_SESSION['role'] = "user"; //TODO : chiffrer ca
            return new User($login,"user");
        }else{
            return null;
        }

    }

    /**
     * @param string $login
     * @param string $mdp
     * @param string $mdpVerif
     * @return array|null
     */
    public function inscription(string $login, string $mdp, string $mdpVerif)
    {
        $login = Validation::sanitizeString($login);
        $mdp = Validation::sanitizeString($mdp);
        $mdpVerif = Validation::sanitizeString($mdpVerif);
        if($mdpVerif != $mdp){
            $err = array();
            $err[] = "Not the same password";
        }
        if($login == "" || $mdp == ""){
            if(!isset($err)){$err = array();}
            $err[] = "Fields cannot be empty";
        }
        $hash = $this->userGateway->checkUser($login);
        if($hash != null){
            if(!isset($err)){$err = array();}
            $err[] = "User already exist";
        }
        if(isset($err)){
            return $err;
        }
        $this->userGateway->addUser($login,$mdp);
        return null;
    }

    /**
     * @return null|User
     */
    public static function isUser(){
        if(isset($_SESSION['login']) && isset($_SESSION['role']) && $_SESSION['role'] == "user"){
            $login = Validation::sanitizeString($_SESSION['login']);
            $role = Validation::sanitizeString($_SESSION['role']);
            return new User($login, $role);
        }else{
            return null;
        }
    }

    /**
     *
     */
    public function deconnexion(){
        session_unset();
        session_destroy();
        $_SESSION = array();
    }

    /**
     * @param $user
     * @return array
     * @throws Exception
     */
    public function all_taskList_private($user)
    {
        $user = Validation::sanitizeString($user);
        return $this->taskListGateway->selectAllPrivate($user);
    }

    /**
     * @param $pageP
     * @param $user
     * @return array
     * @throws Exception
     */
    public function taskList_by_page($pageP, $user)
    {
        global $taskListByPage;
        $user = Validation::sanitizeString($user);
        return $this->taskListGateway->selectByPagePrivate($user,$pageP,$taskListByPage);
    }

    /**
     * @param $log
     * @param $title
     * @param $comment
     */
    public function addPrivateTaskList($log, $title, $comment)
    {
        $this->taskListGateway->insertPrivate($log,$title,$comment);

    }

    /**
     * @param $id
     * @return bool
     */
    public function removeTaskListPrivate($id)
    {
        $this->taskGateway->deleteAllPrivateFromParent($id);
        return $this->taskListGateway->deletePrivate($id);
    }

    /**
     * @param $id
     * @param int $completed
     * @param string $login
     */
    public function changePrivateState($id, int $completed, string $login)
    {
        $this->taskListGateway->changePrivateTaskListState($id,$completed,$login);
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function tasksFromList($id)
    {
        return $this->taskGateway->tasksFromListPrivate($id);
    }

    /**
     * @param $id
     * @return null
     * @throws Exception
     */
    public function taskListFromId($id)
    {
        return $this->taskListGateway->selectByIdPrivate($id);
    }

    /**
     * @param $page
     * @param $parent
     * @return array
     * @throws Exception
     */
    public function task_by_page($page, $parent)
    {
        global $taskByPage;
        return $this->taskGateway->selectByPagePrivate($parent,$page,$taskByPage);
    }

    /**
     * @param $id
     * @param int $completed
     * @param $parent
     */
    public function changePrivateStateTask($id, int $completed, $parent)
    {
        $this->taskGateway->changePrivateTaskState($id,$completed,$parent);
    }

    /**
     * @param $id
     * @return bool
     */
    public function removeTaskPrivate($id)
    {
        return $this->taskGateway->deletePrivate($id);
    }

    /**
     * @param string $title
     * @param string $comment
     * @param $parent
     */
    public function add_private_task(string $title, string $comment, $parent)
    {
        $this->taskGateway->insertPrivate($title,$comment,$parent);
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function all_task_private($id)
    {
        return $this->taskGateway->tasksFromListPrivate($id);
    }


}